<%--
  Created by IntelliJ IDEA.
  User: 86130
  Date: 2021/4/21
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
  <title>自动贩卖机后台管理系统</title>
  <link rel="icon" href="favicon.ico" type="image/ico">
  <meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
  <meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
  <meta name="author" content="yinqi">
  <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>
<body onload="init()">
<div class="lyear-layout-web">
  <div class="lyear-layout-container">
    <!--左侧导航-->
    <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
    <!--End 左侧导航-->

    <!--头部信息-->
    <header class="lyear-layout-header">

      <nav class="navbar navbar-default">
        <div class="topbar">

          <div class="topbar-left">
            <div class="lyear-aside-toggler">
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
            </div>
            <span class="navbar-page-title"> 后台首页 </span>
          </div>
          <%@include file="/pages/static/jsp/rightNavigation.jsp"%>
        </div>
      </nav>

    </header>
    <!--End 头部信息-->

    <!--页面主要内容-->
    <main class="lyear-layout-content">

      <div class="container-fluid">

        <div class="row">
          <div class="col-sm-6 col-lg-3">
            <div class="card bg-primary">
              <div class="card-body clearfix">
                <div class="pull-right">
                  <p class="h6 text-white m-t-0">今日收入</p>
                  <p class="h3 text-white m-b-0 fa-1-5x">102,125.00</p>
                </div>
                <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-currency-cny fa-1-5x"></i></span> </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-3">
            <div class="card bg-danger">
              <div class="card-body clearfix">
                <div class="pull-right">
                  <p class="h6 text-white m-t-0">用户总数</p>
                  <p class="h3 text-white m-b-0 fa-1-5x">920,000</p>
                </div>
                <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-account fa-1-5x"></i></span> </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-3">
            <div class="card bg-success">
              <div class="card-body clearfix">
                <div class="pull-right">
                  <p class="h6 text-white m-t-0">下载总量</p>
                  <p class="h3 text-white m-b-0 fa-1-5x">34,005,000</p>
                </div>
                <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-arrow-down-bold fa-1-5x"></i></span> </div>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-lg-3">
            <div class="card bg-purple">
              <div class="card-body clearfix">
                <div class="pull-right">
                  <p class="h6 text-white m-t-0">新增留言</p>
                  <p class="h3 text-white m-b-0 fa-1-5x">153 条</p>
                </div>
                <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-comment-outline fa-1-5x"></i></span> </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h4>每周用户</h4>
              </div>
              <div class="card-body">
                <canvas class="js-chartjs-bars"></canvas>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h4>交易历史记录</h4>
              </div>
              <div class="card-body">
                <canvas class="js-chartjs-lines"></canvas>
              </div>
            </div>
          </div>

        </div>
        <div class="row" id="map_canvas" style="width: 100%;height: 800px">
        </div>
      </div>
    </main>
    <!--End 页面主要内容-->
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script charset="utf-8" src="https://map.qq.com/api/js?v=2.exp&libraries=visualization,place&&key=RD6BZ-KGRE3-VPT3H-3WFIY-WBF4E-ITFX6"></script>
<%--引入地图资源和echarts--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/echarts.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/map/china.js"></script>
<!--图表插件  引入需要改-->
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/static/js/Initpolish.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/Chart.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript">
  var markerIndex = 0;
  var map;
  //设置一个机器点data 和 热力图的点data
  var machineSitedata = [
    [39.959228, 116.367874],
    [39.984486, 116.427612],
    [39.988169, 116.279984],
    [39.847558, 116.402893],
    [39.941857, 116.383667],
    [40.022882, 116.551208],
    [39.819085, 116.581421],
    [39.924482, 116.205826],
    [39.757880, 116.162567],
    [39.631606, 116.325989],
    [39.797986, 116.415253],
    [40.117990, 116.416626],
    [40.271668, 116.638412],
    [40.143190, 116.236038],
    [39.928168, 116.515503],
    [39.902362, 116.389160],
    [39.935539, 116.377487]
  ];
  // 指定图表的配置项和数据
  option = {
    title: {
      text: '国内售出数量图',
      left: 'center'
    },
    tooltip: {
      trigger: 'item'
    },
    legend: {
      orient: 'vertical',
      left: 'left',
      data: ['中国疫情图']
    },
    visualMap: {
      type: 'piecewise',
      pieces: [
        { min: 1000, max: 1000000, label: '大于等于1000', color: '#372a28' },
        { min: 500, max: 999, label: '售出500-999', color: '#4e160f' },
        { min: 100, max: 499, label: '售出100-499', color: '#974236' },
        { min: 10, max: 99, label: '售出10-99', color: '#ee7263' },
        { min: 1, max: 9, label: '售出1-9', color: '#f5bba7' },
      ],
      color: ['#E0022B', '#E09107', '#A3E00B']
    },
    toolbox: {
      show: true,
      orient: 'vertical',
      left: 'right',
      top: 'center',
    },
    roamController: {
      show: true,
      left: 'right',
      mapTypeControl: {
        'china': true
      }
    },
    series: [
      {
        name: '售出数',
        type: 'map',
        mapType: 'china',
        roam: false,
        label: {
          show: true,
          color: 'rgb(249, 249, 249)'
        },
        data: [
          {

            value: 212
          }, {
            name: '天津',
            value: 60
          }, {
            name: '上海',
            value: 208
          }, {
            name: '重庆',
            value: 337
          }, {
            name: '河北',
            value: 126
          }, {
            name: '河南',
            value: 675
          }, {
            name: '云南',
            value: 117
          }, {
            name: '辽宁',
            value: 74
          }, {
            name: '黑龙江',
            value: 155
          }, {
            name: '湖南',
            value: 593
          }, {
            name: '安徽',
            value: 480
          }, {
            name: '山东',
            value: 270
          }, {
            name: '新疆',
            value: 29
          }, {
            name: '江苏',
            value: 308
          }, {
            name: '浙江',
            value: 829
          }, {
            name: '江西',
            value: 476
          }, {
            name: '湖北',
            value: 13522
          }, {
            name: '广西',
            value: 139
          }, {
            name: '甘肃',
            value: 55
          }, {
            name: '山西',
            value: 74
          }, {
            name: '内蒙古',
            value: 34
          }, {
            name: '陕西',
            value: 142
          }, {
            name: '吉林',
            value: 42
          }, {
            name: '福建',
            value: 179
          }, {
            name: '贵州',
            value: 56
          }, {
            name: '广东',
            value: 797
          }, {
            name: '青海',
            value: 15
          }, {
            name: '西藏',
            value: 1
          }, {
            name: '四川',
            value: 282
          }, {
            name: '宁夏',
            value: 34
          }, {
            name: '海南',
            value: 79
          }, {
            name: '台湾',
            value: 10
          }, {
            name: '香港',
            value: 15
          }, {
            name: '澳门',
            value: 9
          }
        ]
      }
    ]
  };
  window.init = function() {
    var Map = qq.maps.Map;
    var Marker = qq.maps.Marker;
    var LatLng = qq.maps.LatLng;
    var Event = qq.maps.event;
    var MVCArray = qq.maps.MVCArray;
    var MarkerCluster = qq.maps.MarkerCluster;
    //默认地图中心设置为背景,设置地图的属性
    var latlng = new LatLng(39.91, 116.38);
    var options = {
      'zoom':16,
      maxZoom:16,
      minZoom:10,
      'center':latlng,
      'mapTypeId':"roadmap",
      keyboardShortcuts : false, //设置禁止通过键盘控制地图。默认情况下启用键盘快捷键。
      scrollwheel : true,        //设置滚动缩放默认不允许
      mapStyleId : 'style1'
    };
    //创建地图
    var map = new Map(document.getElementById('map_canvas'), options);
    //MVCArray类似于栈
    var markers = new MVCArray();
    var markerCluster;
    //设置Marker自定义图标的属性，size是图标尺寸，该尺寸为显示图标的实际尺寸，origin是切图坐标，该坐标是相对于图片左上角默认为（0,0）的相对像素坐标，anchor是锚点坐标，描述经纬度点对应图标中的位置
    //设置marks的图片等属性
    var anchor = new qq.maps.Point(0, 39),
        size = new qq.maps.Size(50, 50),
        origin = new qq.maps.Point(0, 0),
        icon = new qq.maps.MarkerImage(
            "${pageContext.request.contextPath}/pages/images/machine/自动售货机%20(2).png",
            size,
            origin,
            anchor
        );
    //给mark创建信息窗口
    //添加信息窗口
    var info = new qq.maps.InfoWindow({
      map: map
    });
    let mapContainer=document.getElementById('map_canvas');
    //创建关闭地图时的文字图层
    let closeMapButtonStyle = "padding:5px;border:2px solid #86acf2;background:#ffffff";
    let closeHotMapButton=document.createElement('div');
    //创建关闭热力图的文字显示图层
    closeHotMapButton.style.cssText=closeMapButtonStyle
    closeHotMapButton.style.margin="0 5px"
    closeHotMapButton.innerHTML="打开热力图显示";
    //创建国内地图图层
    let saleChinaMapDiv=document.createElement('div');
    let saleChinaMapStyle="width:400px;height:400px;background-color: #efe1db"
    saleChinaMapDiv.style.cssText=saleChinaMapStyle;
    saleChinaMapDiv.id='saleChinaMapDiv';
    const saleChinaMap1= echarts.init(saleChinaMapDiv);
    saleChinaMap1.setOption(option)
    //创建文字搜索图层
    let searchMapInput=document.createElement('input');
    let searchMapInputStyle="padding:5px;border:2px solid #86acf2;background:#ffffff"
    searchMapInput.style.cssText=searchMapInputStyle;
    let keyword = "";
    //给搜索图层添加事件
    const ap = new qq.maps.place.Autocomplete(searchMapInput);
    //添加监听事件
    qq.maps.event.addListener(ap, "confirm", function(res){
      keyword = res.value;
      searchService.search(keyword);
    });
    //调用Poi检索类。用于进行本地检索、周边检索等服务。
    var searchService = new qq.maps.SearchService({
      complete : function(results){
        if(results.type === "CITY_LIST") {
          searchService.setLocation(results.detail.cities[0].cityName);
          searchService.search(keyword);
          return;
        }
        var pois = results.detail.pois;
        var latlngBounds = new qq.maps.LatLngBounds();
        for(var i = 0,l = pois.length;i < l; i++){
          var poi = pois[i];
          latlngBounds.extend(poi.latLng);
        }
        map.fitBounds(latlngBounds);
      }
    });

    //遍历data中的数据并且放入markers
    function createCluster() {
      let i;
      for (i = 0; i < machineSitedata.length; i++) {
        (function (n) {
          var latLng = new LatLng(machineSitedata[n][0], machineSitedata[n][1]);
          var marker = new Marker({
            position:latLng,
            map:map,
            icon: icon,
            animation: qq.maps.MarkerAnimation.BOUNCE
          });
          markers.push(marker);
          qq.maps.event.addListener(marker, 'click', function() {
            marker.setIcon(icon);//更改当前点击marker样式
            info.open();
            info.setContent('<div style="text-align:center;white-space:'+
                'nowrap;margin:10px;">这是 ' +
                n + ' 号机器</div>');
            info.setPosition(latLng);
          });
        })(i);
      }
      //创建一个点聚合的对象
      markerClusterer = new MarkerCluster({
        map:map, //设置点聚合的地图对象
        minimumClusterSize:2, //默认2
        markers:markers, //将所有点都添加到点聚合对象当中来
        zoomOnClick:true, //默认为true 点击已经聚合的标记点实现聚合分离
        gridSize:60, //默认60
        averageCenter:true, //默认false
        maxZoom:16, //默认18
      });
    };
    //实现热力图部分，创建一个热力图对象
    var heat = new qq.maps.visualization.Heat({
      map: map, // 必填参数，指定显示热力图的地图对象
      radius: 30, // 辐射半径，默认为20
      //设置热力图颜色
      gradient: {
        0 : "#04fafa",
        0.1 : "#04fafa",
        0.2 : "#04fafa",
        0.3 : "#04fafa",
        0.4 : "#04fafa",
        0.5 : "#04fafa",
        0.6 : "#04fafa",
        0.7 : "#04fafa",
        0.8 : "#04fafa",
        0.9 : "#04fafa",
        1.0 : "#04fafa"
      }
    });
    // 获取热力数据
    var data = getHeatData();
    // 向热力图传入数据
    heat.setData(data);
    heat.hide();
    //给按钮添加事件实现点击隐藏/显示热力图
    qq.maps.event.addDomListener(closeHotMapButton,'click',function (event) {
      if(heat.visible){
        heat.hide();
      }else{
        heat.show();
      }
    })
    //将关闭地图的按钮添加到map对象当中
    map.controls[qq.maps.ControlPosition.TOP_LEFT].push(saleChinaMapDiv);
    map.controls[qq.maps.ControlPosition.TOP_CENTER].push(searchMapInput);
    map.controls[qq.maps.ControlPosition.TOP_CENTER].push(closeHotMapButton);
    //遍历data中的数据并且放入markers
    createCluster();
    //设置点的随机位置
    function getHeatData(cnt, max, min) {
      let data = [];
      let center = {
        lat: 39.9,
        lng: 116.4
      };
      cnt = cnt || 100;
      max = max || 100;
      min = min || 0;
      for (let index = 0; index < machineSitedata.length; index++) {
        let r = Math.random();
        let angle = Math.random() * Math.PI * 2;
        let heatValue = Math.random() * (max - min) + min;
        data.push({
          lat: machineSitedata[index][0],
          lng: machineSitedata[index][1],
          value: heatValue
        });
      }
      return {
        max: max,
        min: min,
        data: data
      };
    }
  };
  $(document).ready(function(e) {
    var $dashChartBarsCnt  = jQuery( '.js-chartjs-bars' )[0].getContext( '2d' ),
        $dashChartLinesCnt = jQuery( '.js-chartjs-lines' )[0].getContext( '2d' );

    var $dashChartBarsData = {
      labels: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
      datasets: [
        {
          label: '注册用户',
          borderWidth: 1,
          borderColor: 'rgba(0,0,0,0)',
          backgroundColor: 'rgba(51,202,185,0.5)',
          hoverBackgroundColor: "rgba(51,202,185,0.7)",
          hoverBorderColor: "rgba(0,0,0,0)",
          data: [2500, 1500, 1200, 3200, 4800, 3500, 1500]
        }
      ]
    };
    var $dashChartLinesData = {
      labels: ['2021-4', '2021-3', '2021-2', '2021-1', '2020-12', '2020-11', '2020-10', '2020-9', '2020-8', '2020-7', '2020-6', '2020-5'],
      datasets: [
        {
          label: '交易资金',
          data: [20, 25, 40, 30, 45, 40, 55, 40, 48, 40, 42, 50],
          borderColor: '#358ed7',
          backgroundColor: 'rgba(53, 142, 215, 0.175)',
          borderWidth: 1,
          fill: false,
          lineTension: 0.5
        }
      ]
    };

    new Chart($dashChartBarsCnt, {
      type: 'bar',
      data: $dashChartBarsData
    });

    var myLineChart = new Chart($dashChartLinesCnt, {
      type: 'line',
      data: $dashChartLinesData,
    });
    // 使用时，请自行引入jquery.cookie.js
    // 读取cookie中的主题设置
    var the_logo_bg    = $.cookie('the_logo_bg'),
        the_header_bg  = $.cookie('the_header_bg'),
        the_sidebar_bg = $.cookie('the_sidebar_bg'), // iframe版本如果删除了下面一行，请把这一行的逗号改成分号
        the_site_theme = $.cookie('the_site_theme'); // iframe版本可不需要这行

    if (the_logo_bg) $('body').attr('data-logobg', the_logo_bg);
    if (the_header_bg) $('body').attr('data-headerbg', the_header_bg);
    if (the_sidebar_bg) $('body').attr('data-sidebarbg', the_sidebar_bg);
    if (the_site_theme) $('body').attr('data-theme', the_site_theme); // iframe版本可不需要这行

    // 处理主题配色下拉选中
    $(".dropdown-skin :radio").each(function(){
      var $this = $(this),
          radioName = $this.attr('name');
      switch (radioName) {
        case 'site_theme':
          $this.val() == the_site_theme && $this.prop("checked", true);
          break;  // iframe版中不需要这个case
        case 'logo_bg':
          $this.val() == the_logo_bg && $this.prop("checked", true);
          break;
        case 'header_bg':
          $this.val() == the_header_bg && $this.prop("checked", true);
          break;
        case 'sidebar_bg':
          $this.val() == the_sidebar_bg && $this.prop("checked", true);
      }
    });

    // 设置主题配色
    setTheme = function(input_name, data_name) {
      $("input[name='"+input_name+"']").click(function(){
        $('body').attr(data_name, $(this).val());
        $.cookie('the_'+input_name, $(this).val());
      });
    }
    setTheme('site_theme', 'data-theme'); // iframe版本可不需要这行
    setTheme('logo_bg', 'data-logobg');
    setTheme('header_bg', 'data-headerbg');
    setTheme('sidebar_bg', 'data-sidebarbg');
  });
</script>
</body>
</html>
