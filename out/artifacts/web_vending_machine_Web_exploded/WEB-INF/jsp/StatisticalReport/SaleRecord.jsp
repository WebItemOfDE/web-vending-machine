<%--
  Created by IntelliJ IDEA.
  User: 86130
  Date: 2021/8/7
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>销售记录</title>
  <link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
  <meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
  <meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
  <meta name="author" content="yinqi">
  <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
  <!--  引入时间选择器和日期选择器插件-->
  <!--时间选择插件-->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/pages/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
  <!--日期选择插件-->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/pages/js/bootstrap-datepicker/bootstrap-datepicker3.min.css">

</head>
<body>
<div class="lyear-layout-web">
  <div class="lyear-layout-container">
    <!--左侧导航-->
    <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
    <!--End 左侧导航-->

    <!--头部信息-->
     <header class="lyear-layout-header">

      <nav class="navbar navbar-default">
        <div class="topbar">

          <div class="topbar-left">
            <div class="lyear-aside-toggler">
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
            </div>
            <span class="navbar-page-title"> 后台首页 </span>
          </div>
          <%@include file="/pages/static/jsp/rightNavigation.jsp"%>
        </div>
      </nav>

    </header>
    <!--End 头部信息-->

    <!--页面主要内容-->
     <main class="lyear-layout-content">
      <div class="container-fluid">
        <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <button class="btn btn-warning dropdown-toggle" id="searchSaleRecord-btn" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false" style="float: right">
                      <i class="mdi mdi-search-web"></i>  查找
                    </button>
                    <h4 style="margin-top: 5px">日期选择</h4>
                    <form class="form-inline" action="lyear_forms_elements.html" method="post" onsubmit="return false;" style="float: right">
                      <div class="form-group">
                        <label class="col-xs-4" for="saleRecord_dname_input" style="margin-top: 5px">商品名:</label>
                        <div class="col-xs-8">
                          <input class="form-control"  id="saleRecord_dname_input" name="example-if-email" placeholder="">
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="card-body">
                    <div class="input-daterange input-group js-datetimepicker" data-auto-close="false" data-date-format="YYYY-MM-DD HH:mm:ss">
                      <input class="form-control js-datetimepicker" type="text" id="startTime_input" name="add_time" placeholder="开始时间" value="" data-side-by-side="true" data-locale="zh-cn" data-date-format="YYYY-MM-DD HH:mm:ss" />
                      <span class="input-group-addon"><i class="mdi mdi-chevron-right"></i></span>
                      <input class="form-control js-datetimepicker" type="text" id="endTime_input" name="add_time" placeholder="结束时间" value="" data-side-by-side="true" data-locale="zh-cn" data-date-format="YYYY-MM-DD HH:mm:ss" />
                    </div>
                  </div>
                  <div class="card-body">

                    <div class="table-responsive">
                      <table class="table table-bordered">
                        <thead>
                        <tr>
                          <th>
                            <label class="lyear-checkbox checkbox-primary">
                              <input type="checkbox" id="check-all"><span></span>
                            </label>
                          </th>
                          <th>订单号</th>
                          <th>药品名</th>
                          <th>机器编号</th>
                          <th>销售数量</th>
                          <th>销售价格</th>
                          <th>销售日期</th>
                          <th>销售用户</th>
                        </tr>
                        </thead>
                        <tbody id="SaleRecord_tbody">

                        </tbody>
                      </table>
                    </div>
                    <div class="clearfix">
                      <div class="form-group " style="float: left">
                        <btn class="btn btn-danger" id="Allsales_delete"><i class="mdi mdi-window-close"></i>删除</btn>
                      </div>
                    </div>
                    <ul class="pagination" id="SaleRecord_pageInfo_area">
                    </ul>
                  </div>
                </div>
              </div>
        </div>

      </div>

    </main>
    <!--End 页面主要内容-->
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/static/js/Initpolish.js"></script>
<!--时间选择插件-->
<script src="${pageContext.request.contextPath}/pages/js/bootstrap-datetimepicker/moment.min.js"></script>
<script src="${pageContext.request.contextPath}/pages/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/pages/js/bootstrap-datetimepicker/locale/zh-cn.js"></script>
<!--日期选择插件-->
<script src="${pageContext.request.contextPath}/pages/js/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/pages/js/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js"></script>
<script type="text/javascript">
  'use strict'
  var totalRecord,currentPage;
  $(function () {
    to_page(1);
  })
  function to_page(pn){
    $.ajax({
      url:"${pageContext.request.contextPath}/sale/findSaleAll",
      data:"pn="+pn,
      type:"Get",
      success:function (result) {
        // console.log(result);
        //遍历数据显示在表格中
        builld_saleRecord_table(result);
        builld_saleRecord_pageInfo_area(result);
      }
    })
  }
  //构建表格数据
  function builld_saleRecord_table(result){
    let drugs = result.extend.PageInfo.list;
    let html="";
    let checkBoxId="saleTableCheck";
    let date,date1;
    let deleteId="delete";
    let changeId="change";
    $.each(drugs,function (index,item) {
      checkBoxId=checkBoxId+item.orderNumber;
      //日期转换
      date= getdate(item.saleDate);
      deleteId=deleteId+item.orderNumber;
      changeId=changeId+item.orderNumber;
      html=html+
          "<tr>"
          +"<td>"+"<label class='lyear-checkbox checkbox-primary'>"+"<input type='checkbox' name='ids' value='"+index+"' class='check_item' id='"+checkBoxId+"'>"+"<span>"+"</span>"+"</label>"+"</td>"
          +"<td id='drid"+item.orderNumber+"'>"+item.orderNumber+"</td>"
          +"<td>"+item.dname+"</td>"
          +"<td>"+item.mhid+"</td>"
          +"<td>"+item.salePrice+"</td>"
          +"<td>"+item.saleNumber+"</td>"
          +"<td>"+date+"</td>"
          +"<td>"+item.saleUseropenid+"</td>"
          +"<td>"
          +"<div class='btn btn-group'>"+
            "<a class='btn btn-xs btn-default deleteSales_btn' id='"+deleteId+"' title='删除' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-window-close'></i>"+"</a>"+
          "</div>"
          +"</td>"+
          +"</tr>"
      checkBoxId="saleTableCheck";
      deleteId="delete";
      changeId="change";
    })
    $('#SaleRecord_tbody').html(html);
  }
  //构建分页区域
  function builld_saleRecord_pageInfo_area(result) {
    let html="";
    //判断还有没有上一页
    if(result.extend.PageInfo.hasPreviousPage==false){
      html=html+"<li class='disabled'  >"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
    }else {
      html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum-1)+")'>"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
    }
    //遍历得到页码号
    $.each(result.extend.PageInfo.navigatepageNums,function (index,item) {
      //判断是不是当前页
      if(result.extend.PageInfo.pageNum==item){
        html=html+"<li class='active' onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
      }else{
        html=html+"<li onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
      }
    });
    //判断还有没有下一页
    if (result.extend.PageInfo.hasNextPage==false){
      html=html+"<li class='disabled' >"+"<a href='#!'>"+">>"+"</a>"+"</li>";
    }else {
      html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum+1)+")'>"+"<a href='#!'>"+">>"+"</a>"+"</li>";
    }
    $('#SaleRecord_pageInfo_area').html(html);
    totalRecord=result.extend.PageInfo.total;
    currentPage=result.extend.PageInfo.pageNum;
  }

  //解析日期类
  function getdate(jsonDate) {
    var now = new Date(jsonDate),
        y = now.getFullYear(),
        m = now.getMonth() + 1,
        d = now.getDate();
    return y + "-" + (m < 10 ? "0" + m : m) + "-" + (d < 10 ? "0" + d : d) + " " + now.toTimeString().substr(0,8);
  };
  //按日期和商品名进行查询
  $('#searchSaleRecord-btn').click(function () {
    let startDate = $('#startTime_input').val();
    let endDate =$('#endTime_input').val();
    let dname = $('#saleRecord_dname_input').val();
    $.ajax({
      url:"${pageContext.request.contextPath}/sale/findSaleByNameDate",
      type:"POST",
      dataType: "json",
      data:{
        dname: dname,
        startDate: startDate,
        endDate: endDate
      },
      success:function (result) {
        builld_saleRecord_table(result);
        builld_saleRecord_pageInfo_area(result);
      }
    })
  })
  $(function(){
    $('.search-bar .dropdown-menu a').click(function() {
      var field = $(this).data('field') || '';
      $('#search-field').val(field);
      $('#searchSaleRecord-btn').html($(this).text() + ' <span class="caret"></span>');
    });
  });
  //给表格中的删除链接绑定事件
  $(document).on("click",".deleteSales_btn",function () {
    let cid = $(this).attr("id");
    let num=cid.toString().replace("delete","");
    $.ajax({
      url:"${pageContext.request.contextPath}/sale/deleteSaleByNums",
      data:"nums="+num,
      success:function (result) {
        to_page(currentPage);
      }
    })
  })
  //给删除按钮绑定事件,批量删除
  $('#Allsales_delete').click(function () {
    let nums="";
    let nums1="";
    $.each($(".check_item:checked"),function () {
      let num =$(this).attr("id");
      num=num.toString().replace("saleTableCheck","");
      nums+=num+",";
      nums1+=num+"-";
    })
    nums=nums.substring(0,nums.length-1);
    nums1=nums1.substring(0,nums1.length-1);
    if(confirm("确认删除销售编号为["+nums+"]的销售数据吗?")){
      $.ajax({
        url:"${pageContext.request.contextPath}/sale/deleteSaleByNums",
        data:"nums="+nums1,
        success:function (result) {
          to_page(currentPage);
        }
      })
    }
  })
</script>
</body>
</html>
