<%--
  Created by IntelliJ IDEA.
  User: Chen175
  Date: 2021/6/21
  Time: 20:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<title>Title</title>
</head>
<body>
<aside class="lyear-layout-sidebar">
  <!-- logo -->
  <div id="logo">
    <a href="${pageContext.request.contextPath}/user/toIndex"><p style="width: 124px;height: 58px;font-family: AppleSystemUIFont;font-size: 31px;font-weight: normal;font-stretch: normal;line-height: 41px;letter-spacing: 0px;color: #D1F0F1;margin-left: 30px;margin-top:10px">健康药柜</p> </a>
  </div>
  <div class="lyear-layout-sidebar-scroll">
    
    <nav class="sidebar-main">
      <ul class="nav nav-drawer">
        <li class="nav-item "> <a href="${pageContext.request.contextPath}/user/toIndex"><i class="mdi mdi-home"></i> 后台首页</a> </li>
        <!--          自己的内容-->
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-shopping"></i> 编辑商品</a>
          <ul class="nav nav-subnav">
            <li><a href="${pageContext.request.contextPath}/drug/toAllProducts">所有商品</a> </li>
            <li><a href="${pageContext.request.contextPath}/specification/toProductSpecification">商品规格</a> </li>
            <li><a href="">产品设置</a> </li>
            <li><a href="CommodityRecycling.html">商品回收站</a> </li>
          </ul>
        </li>
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-washing-machine"></i>售货机</a>
          <ul class="nav nav-subnav">
            <li><a href="${pageContext.request.contextPath}/machine/ToManagemnet">机器管理</a> </li>
            <li><a href="${pageContext.request.contextPath}/machine/ToManagemnet">商品库存查看</a> </li>
          </ul>
        </li>
        <li class="nav-item nav-item-has-subnav">
          <a href="javascript:void(0)"><i class="mdi mdi-history"></i>统计报表</a>
          <ul class="nav nav-subnav">
            <li><a href="${pageContext.request.contextPath}/presentation/toChartPresentation">图表展示</a> </li>
            <li><a href="${pageContext.request.contextPath}/chart/toAnalysisRecord">分析记录</a> </li>
            <li><a href="${pageContext.request.contextPath}/sale/toSaleRecord">销售记录</a> </li>
          </ul>
        </li>
      </ul>
    </nav>
    <div class="sidebar-footer">
      <p class="copyright">Copyright &copy; 2021. <a target="_blank" href="https://gitee.com/WebItemOfDE/web-vending-machine">自动售卖机</a> All rights reserved.</p>
    </div>
  </div>

</aside>
</body>
</html>
