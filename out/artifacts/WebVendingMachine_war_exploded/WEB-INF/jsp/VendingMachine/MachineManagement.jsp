<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>机器管理</title>
	<link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
	<meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
	<meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
	<meta name="author" content="yinqi">
	<link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>
<body>
<div class="lyear-layout-web">
	<div class="lyear-layout-container">
		<!--左侧导航-->
		<%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
		<!--End 左侧导航-->
		
		<!--头部信息-->
		<header class="lyear-layout-header">
			
			<nav class="navbar navbar-default">
				<div class="topbar">
					
					<div class="topbar-left">
						<div class="lyear-aside-toggler">
							<span class="lyear-toggler-bar"></span>
							<span class="lyear-toggler-bar"></span>
							<span class="lyear-toggler-bar"></span>
						</div>
						<span class="navbar-page-title"> 机器管理 </span>
					</div>
					<%@include file="/pages/static/jsp/rightNavigation.jsp" %>
				</div>
			</nav>
		
		</header>
		<!--End 头部信息-->
		<main class="lyear-layout-content">
			
			<div class="container-fluid">
				
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-toolbar clearfix">
								<form class="pull-right search-bar" method="get" action="#!" role="form">
									<div class="input-group">
										<div class="input-group-btn">
											<input type="hidden" name="search_field" id="search-field" value="title">
											<button class="btn btn-warning dropdown-toggle" id="search-btn" data-toggle="dropdown"
															type="button" aria-haspopup="true" aria-expanded="false">
												<i class="mdi mdi-search-web"></i> 查找
											</button>
										</div>
										<input type="text" class="form-control" value="" name="keyword" id="search_field1" placeholder="请输入商品名称"
													 style="solid-color: yellow">
									</div>
								</form>
								<div class="toolbar-btn-action">
									<btn class="btn btn-primary m-r-5"
										 id="Machine_add"
										 data-toggle="modal">
										<i class="mdi mdi-plus"></i> 新增
									</btn>
									<!--新增模态框-->
									<div class="modal fade" id="Machine_add_modal" tabindex="-1" role="dialog"
											 aria-labelledby="myModalLabel" aria-hidden="true" style="pointer-events: auto">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
																	aria-hidden="true">×
													</button>
													<h4 class="modal-title" id="myModalLabel">
														新增机器界面
													</h4>
												</div>
												<div class="modal-body">
													<form action="${pageContext.request.contextPath}/machine/addMachine" method="post">
														<p>机器编号</p>
														<input type="text" id="add_mhid" class="form-control" value="" name="mhid" placeholder="请输入机器编号"
															   style="solid-color: yellow" required>
														<p>机器名称</p>
														<input type="text" id="add_mhname" class="form-control" value="" name="mhname" placeholder="请输入机器名称"
																	 style="solid-color: yellow" required>
														<p>机器地址</p>
														<input type="text" id="add_mhadrress" class="form-control" value="" name="mhipadrress" placeholder="请输入机器地址"
																	 style="solid-color: yellow" required>
													</form>
												</div>

												<div class="modal-footer">
													<button type="button" class="btn btn-default"
															data-dismiss="modal">关闭
													</button>
													<button type="button" id="machine_modal_add_btn" class="btn btn-primary">
														确认添加
													</button>
												</div>

											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div><!-- /.modal -->
									
									
									
									<!--修改按钮-->
									<btn class="btn btn-success m-r-5" id="AllProducts_change" data-toggle="modal"
											 data-target="#AllProducts_change_modal"><i class="mdi mdi-grease-pencil"></i> 修改
									</btn>
									<!--                                未获取修改的项目的id-->
									<div class="modal fade" id="AllProducts_change_modal" tabindex="-1" role="dialog"
											 aria-labelledby="myModalLabel" aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
																	aria-hidden="true">×
													</button>
													<h4 class="modal-title" id="myModalLabel1">
														机器信息修改
													</h4>
												</div>
												<div class="modal-body">
													<form>
														<p>机器编号</p>
														<input type="text" readonly="readonly" class="form-control" value="" name="mhid" id="MachineId_update_input" placeholder="请输入机器名称"
															   style="solid-color: yellow">
														<p>机器名称</p>
														<input type="text" class="form-control" value="" name="mhname" id="MachineName_update_input" placeholder="请输入机器名称"
																	 style="solid-color: yellow">
														<p>机器地址</p>
														<input type="text" class="form-control" value="" name="mhipadrress" id="MachineAdrress_update_input" placeholder="请输入机器名称"
																	 style="solid-color: yellow">
													</form>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default"
																	data-dismiss="modal">关闭
													</button>
													<button type="button" id="Machine_change_modal_btn" class="btn btn-primary">
														确认修改
													</button>
												</div>
											</div><!-- /.modal-content -->
										</div><!-- /.modal-dialog -->
									</div><!-- /.modal -->
									<btn class="btn btn-danger m-r-5" id="AllProducts_delete"><i class="mdi mdi-window-close"></i> 删除
									</btn>
								</div>
							</div>
							<div class="card-body">
								
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
										<th>
											<label class="lyear-checkbox checkbox-primary">
												<input type="checkbox" id="check-all"><span></span>
											</label>
										</th>
										<th>机器编号</th>
										<th>机器名称</th>
										<th>机器地址</th>
										<th>操作</th>
										</thead>
										<tbody id="Machine_tbody">
										</tbody>
									</table>
								</div>
								<ul class="pagination" id="machine_pageInfo_area">
								</ul>
							</div>
						</div>
					</div>
				
				</div>
			
			</div>
		</main>
	</div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/static/js/Initpolish.js"></script>


<script type="text/javascript">
	'use strict'
	var totalRecord,currentPage;
	//导入
	$(function () {
		to_page(1);
	});
	function to_page(pn){
		$.ajax({
			url:"${pageContext.request.contextPath}/machine/AllMachine",
			data:"pn="+pn,
			type:"Get",
			success:function (result) {
				//遍历数据显示在表格中
				builld_Machine_table(result);
				builld_Machine_pageInfo_area(result);
			}
		})
	}
	//构建表格数据
	function builld_Machine_table(result){
		let html="";
		let Machines=result.extend.PageInfo.list;
		let checkBoxId="machineTableCheck";
		let deleteId="delete";
		let changeId="change";
		$.each(Machines,function (index,item) {
			checkBoxId=checkBoxId+item.mhid;
			deleteId=deleteId+item.mhid;
			changeId=changeId+item.mhid;
			html=html+
							"<tr>"
							+"<td>"+"<label class='lyear-checkbox checkbox-primary'>"+"<input type='checkbox' name='ids' value='"+index+"'  class='check_item' id='"+checkBoxId+"'>"+"<span>"+"</span>"+"</label>"+"</td>"
							+"<td id='mhid"+item.mhid+"' >"+item.mhid+"</td>"
							+"<td>"+item.mhname+"</td>"
							+"<td>"+item.mhipadrress+"</td>"
							+"<td>"
							+"<div class='btn btn-group'>"+
							"<a class='btn btn-xs btn-default editMachine_Moralbtn' id='"+changeId+"' title='编辑' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-pencil'></i>"+"</a>"+
<%--							"<a class='btn btn-xs btn-default selectMachine_Moralbtn' id='"+selectId+"' title='查看' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-eye'></i>"+"</a>"+--%>
                            "<a class='btn btn-xs btn-default' title='查看' data-toggle='tooltip' href='"+"${pageContext.request.contextPath}/inventory/toAllProducts?mhid="+item.mhid+"'>"+"<i class='mdi mdi-eye'></i>"+"</a>"+
							"<a class='btn btn-xs btn-default deleteMachine_Moralbtn' id='"+deleteId+"' title='删除' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-window-close'></i>"+"</a>"+
							"</div>"
							+"</td>"+
							+"</tr>"
			checkBoxId="machineTableCheck";
			deleteId="delete";
			changeId="change";
		})
		$('#Machine_tbody').html(html);
	}
	//构建导航栏
	function builld_Machine_pageInfo_area(result) {
		let html="";
		//判断还有没有上一页
		if(result.extend.PageInfo.hasPreviousPage==false){
			html=html+"<li class='disabled'  >"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
		}else {
			html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum-1)+")'>"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
		}
		//遍历得到页码号
		$.each(result.extend.PageInfo.navigatepageNums,function (index,item) {
			//判断是不是当前页
			if(result.extend.PageInfo.pageNum==item){
				html=html+"<li class='active' onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
			}else{
				html=html+"<li onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
			}
		});
		//判断还有没有下一页
		if (result.extend.PageInfo.hasNextPage==false){
			html=html+"<li class='disabled' >"+"<a href='#!'>"+">>"+"</a>"+"</li>";
		}else {
			html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum+1)+")'>"+"<a href='#!'>"+">>"+"</a>"+"</li>";
		}
		$('#machine_pageInfo_area').html(html);
		totalRecord=result.extend.PageInfo.total;
		currentPage=result.extend.PageInfo.pageNum;
	}

	//给按钮绑定单击事件
	window.onload=function (){
		$('#searchVeterinarianByName-btn').click(function () {
			let elementById = document.getElementById("searchVeterinarianByName_field");
			let name = elementById.value;
			to_page_ByName(name);
		})
	}


	//按条件查询ajax
    window.onload=function () {
        $('#search-btn').click(function () {
            let file = document.getElementById("search_field1");
            let value = file.value;
            to_page_select(value,1);
        })
    }

	//切换到第几页
    function to_page_select(value,pn) {
        let data={pn:'',mhname:''+value};
        $.ajax({
            url: "${pageContext.request.contextPath}/machine/selecProductsByCondition",
            data:data,
            dataType:'json',
            contentType : "application/json;charset=UTF-8",
            success:function (result) {
                builld_Machine_table(result);
            }
        })
    }

	//给新增机器 模态框增加方法
    $('#Machine_add').click(function () {
        $('#Machine_add_modal').modal({
            backdrop:"static"
        });
    })

	//校验表单非空
    function check_Add_form_Data() {
        //校验
        let machineId = document.getElementById("add_mhid").value;
        let machineName = document.getElementById("add_mhname").value;
        let machineAdrress = document.getElementById("add_mhadrress").value;
        if(machineId==null)
        {
            $('#add_mhid').parent().addClass("has-error");
            $('#add_mhid').next("span").text("机器编号不能为空");
            return false;
        }else {
            $('#add_mhid').parent().addClass("has-success");
            $('#add_mhid').next("span").text("");
        }

        if(machineName==null)
        {
            $('#add_mhname').parent().addClass("has-error");
            $('#add_mhname').next("span").text("机器名称不能为空");
            return false;
        }else {
            $('#add_mhname').parent().addClass("has-success");
            $('#add_mhname').next("span").text("");
        }

        if(machineAdrress==null)
        {
            $('#add_mhadrress').parent().addClass("has-error");
            $('#add_mhadrress').next("span").text("机器地址不能为空");
            return false;
        }else {
            $('#add_mhadrress').parent().addClass("has-success");
            $('#add_mhadrress').next("span").text("");
        }
        return  true;
    }

	//新增机器 模态框点击事件
    $('#machine_modal_add_btn').click(function () {
       if(check_Add_form_Data()==false){}
       else {
           $.ajax({
               url: "${pageContext.request.contextPath}/machine/addMachine",
               type: 'POST',
               data: $('#Machine_add_modal form').serialize(),
               success:function (result) {
                   alert(result.msg);
                   $('#Machine_add_modal').modal('hide');
                   to_page(Math.ceil(totalRecord/5));
               }
           })
       }
    })

     //验证id是否重复
    $('#add_mhid1').change(function () {
        $.ajax({
            url:"${pageContext.request.contextPath}/machine/checkMachine",
            data:"mhid="+this.value,
            type:'POST',
            success:function (result) {
                if(result.code==100){  //成功
                    $('#add_resultP').hide()
                }else if(result.code=200){ //失败
                   $('#add_resultP').show();
                }
            }
        })
    });

    //通过类名给表格中的编辑连接绑定事件
    $(document).on("click",".editMachine_Moralbtn",function () {
        // vid 获取此项的id
        let  cid = $(this).attr("id");
        cid  = cid.toString().replace("change","");
        //rid即为要获取的id
        let mhid=$("#mhid"+cid).text();
        getEmp(mhid)
        //将vid放在修改按钮的属性上,这样可以让后端通过vid查找要修改医师
        $('#Machine_change_modal_btn').attr("edit_mhid",mhid)
        $('#AllProducts_change_modal').modal({
            backdrop:"static"
        })
    })
    //拿到点击后表格中id的值,使用Ajax查询信息显示在模态框中
    function getEmp(mhid) {
        $.ajax({
            url:"${pageContext.request.contextPath}/machine/findMachineByIds",
            data:"mhid="+mhid,
            success:function (result) {
                let machine=result.extend.machine;
                $.each(machine,function (index,item) {
                    $('#MachineId_update_input').val(item.mhid);
                    $('#MachineName_update_input').val(item.mhname);
                    $('#MachineAdrress_update_input').val(item.mhipadrress);
                     })
            }
        })
    }
    //根据模态框中的修改按钮修改
    $('#Machine_change_modal_btn').click(function () {
        let mhid=$(this).attr("edit_mhid");
        $.ajax({
            url:"${pageContext.request.contextPath}/machine/updateMachine",
            data:$('#AllProducts_change_modal form').serialize(),
            success:function () {
                $('#AllProducts_change_modal').modal('hide');
                to_page(currentPage);
            }
        })
    })

    //给表格中的删除链接绑定事件
    $(document).on("click",".deleteMachine_Moralbtn",function () {
        let cid = $(this).attr("id");
        cid=cid.toString().replace("delete","");
        let mhid = $('#mhid'+cid).text();
        $.ajax({
            url:"${pageContext.request.contextPath}/machine/deleteMachine",
            data:"mhid="+mhid,
            success:function (result) {
                to_page(currentPage);
            }
        })
    })
    //给删除按钮绑定事件,批量删除
    $('#AllProducts_delete').click(function () {
        let mhids="";
        let mhids1="";
        $.each($(".check_item:checked"),function () {
            let cid =$(this).attr("id");
            cid=cid.toString().replace("machineTableCheck","");
            let mhid=$('#mhid'+cid).text();
            mhids+=mhid+",";
            mhids1+=mhid+"-";
        })
        mhids=mhids.substring(0,mhids.length-1);
        mhids1=mhids1.substring(0,mhids1.length-1);
        if(confirm("确认删除机器编号为["+mhids+"]的机器吗?")){
            $.ajax({
                url:"${pageContext.request.contextPath}/machine/deleteMachineByIds",
                data:"mhids="+mhids1,
                success:function (result) {
                    to_page(currentPage);
                }
            })
        }
    })

	$(function () {
		$('.search-bar .dropdown-menu a').click(function () {
			var field = $(this).data('field') || '';
			$('#search-field').val(field);
			$('#search-btn').html($(this).text() + ' <span class="caret"></span>');
		});
	});

	$(document).ready(function (e) {
		// 使用时，请自行引入jquery.cookie.js
		// 读取cookie中的主题设置
		var the_logo_bg = $.cookie('the_logo_bg'),
						the_header_bg = $.cookie('the_header_bg'),
						the_sidebar_bg = $.cookie('the_sidebar_bg'), // iframe版本如果删除了下面一行，请把这一行的逗号改成分号
						the_site_theme = $.cookie('the_site_theme'); // iframe版本可不需要这行

		if (the_logo_bg) $('body').attr('data-logobg', the_logo_bg);
		if (the_header_bg) $('body').attr('data-headerbg', the_header_bg);
		if (the_sidebar_bg) $('body').attr('data-sidebarbg', the_sidebar_bg);
		if (the_site_theme) $('body').attr('data-theme', the_site_theme); // iframe版本可不需要这行

		// 处理主题配色下拉选中
		$(".dropdown-skin :radio").each(function () {
			var $this = $(this),
							radioName = $this.attr('name');
			switch (radioName) {
				case 'site_theme':
					$this.val() == the_site_theme && $this.prop("checked", true);
					break;  // iframe版中不需要这个case
				case 'logo_bg':
					$this.val() == the_logo_bg && $this.prop("checked", true);
					break;
				case 'header_bg':
					$this.val() == the_header_bg && $this.prop("checked", true);
					break;
				case 'sidebar_bg':
					$this.val() == the_sidebar_bg && $this.prop("checked", true);
			}
		});

		// 设置主题配色
		setTheme = function (input_name, data_name) {
			$("input[name='" + input_name + "']").click(function () {
				$('body').attr(data_name, $(this).val());
				$.cookie('the_' + input_name, $(this).val(),{
					path:"/"
				});
			});
		}
		setTheme('site_theme', 'data-theme'); // iframe版本可不需要这行
		setTheme('logo_bg', 'data-logobg');
		setTheme('header_bg', 'data-headerbg');
		setTheme('sidebar_bg', 'data-sidebarbg');
	});
</script>
</body>
</html>