<%--
  Created by IntelliJ IDEA.
  User: 86130
  Date: 2021/8/1
  Time: 10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>商品规格</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
  <link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
  <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>
<body>
<div class="lyear-layout-web">
  <div class="lyear-layout-container">
    <!--左侧导航-->
    <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
    <!--End 左侧导航-->

    <!--头部信息-->
    <header class="lyear-layout-header">

      <nav class="navbar navbar-default">
        <div class="topbar">

          <div class="topbar-left">
            <div class="lyear-aside-toggler">
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
              <span class="lyear-toggler-bar"></span>
            </div>
            <span class="navbar-page-title"> 后台首页 </span>
          </div>
          <%@include file="/pages/static/jsp/rightNavigation.jsp" %>
        </div>
      </nav>

    </header>
    <!--End 头部信息-->

    <!--页面主要内容-->
    <main class="lyear-layout-content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-toolbar clearfix">
                <form class="pull-right search-bar" method="get" action="#!" role="form">
                  <div class="input-group">
                    <div class="input-group-btn">
                      <input type="hidden" name="search_field" id="search-field" value="title">
                      <button class="btn btn-warning dropdown-toggle" id="searchSpecificationsByName-btn" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-search-web"></i>  查找
                      </button>
                    </div>
                    <input id="searchSpecificationsByName_field" type="text" class="form-control" value="" name="keyword" placeholder="请输入商品名称" style="solid-color: yellow">
                  </div>
                </form>
                <div class="toolbar-btn-action">
                  <btn class="btn btn-primary m-r-5" id="ProductSpecification_add_btn" data-toggle="modal" data-target="#ProductSpecification_add_modal"><i class="mdi mdi-plus"></i> 新增</btn>
                  <!--新增模态框-->
                  <div class="modal fade" id="ProductSpecification_add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"
                                  aria-hidden="true">×
                          </button>
                          <h4 class="modal-title" id="myaddModalLabel">
                            添加商品
                          </h4>
                        </div>
                        <div class="modal-body">
                          <form enctype="multipart/form-data" id="specification_add_form_moral">
                            <p>商品名称</p>
                            <input  type="text" class="form-control" value="" id="specificationDrname_add_input"  name="drname" placeholder="请输入商品名称" style="solid-color: yellow">
                            <p>商品规格</p>
                            <input  type="text" class="form-control" value="" id="specificationDrspecifications_add_input"  name="drspecifications" placeholder="请输入商品规格" style="solid-color: yellow">
                            <p>商品作用说明</p>
                            <textarea class="form-control" id="specificationDrfunction_add_input" name="drfunction" rows="6" placeholder="内容.."></textarea>
                            <p>商品作用简介</p>
                            <input  type="text" class="form-control" value="" id="specificationDrintroduction_add_input" name="drintroduction" placeholder="请输入商品作用简介" style="solid-color: yellow">
                            <p>商品图片</p>
                            <input type="file" name="dimgurl"  id="specificationdImg_add_input"/><span></span>
                            <div>
                              <img src=""  id="specification_add_Img" alt="no img" width="64" height="64"/>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default"
                                  data-dismiss="modal">关闭
                          </button>
                          <button type="button" class="btn btn-primary" id="ProductSpecification_add_moral_btn">
                            确认添加
                          </button>
                        </div>
                      </div><!-- /.modal-content -->
                    </div>
                  </div><!-- /.modal -->
                  <!--                                未获取修改的项目的id-->
                  <div class="modal fade" id="ProductSpecification_change_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"
                                  aria-hidden="true">×
                          </button>
                          <h4 class="modal-title" id="mychangeModalLabel">
                            修改商品
                          </h4>
                        </div>
                        <div class="modal-body">
                          <form enctype="multipart/form-data" id="specification_change_form_moral">
                            <p>商品名称</p>
                            <input  type="text" class="form-control" value="" id="specificationDrname_change_input"  name="drname" placeholder="请输入商品名称" style="solid-color: yellow" readonly >
                            <p>商品规格</p>
                            <input  type="text" class="form-control" value="" id="specificationDrspecifications_change_input"  name="drspecifications" placeholder="请输入商品规格" style="solid-color: yellow">
                            <p>商品作用说明</p>
                            <textarea class="form-control" id="specificationDrfunction_change_input" name="drfunction" rows="6" placeholder="内容.."></textarea>
                            <p>商品作用简介</p>
                            <input  type="text" class="form-control" value="" id="specificationDrintroduction_change_input" name="drintroduction" placeholder="请输入商品作用简介" style="solid-color: yellow">
                            <p>商品图片</p>
                            <input type="file" name="dimgurl"  id="specificationdImg_change_input"/><span></span>
                            <div>
                              <img src=""  id="specification_change_Img" width="64" height="64"/>
                            </div>
                          </form>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default"
                                  data-dismiss="modal">关闭
                          </button>
                          <button type="button" class="btn btn-primary" id="ProductSpecification_change_moral_btn">
                            确认修改
                          </button>
                        </div>
                      </div><!-- /.modal-content -->
                    </div>
                  </div><!-- /.modal -->
                  <btn class="btn btn-danger" id="AllSpecification_delete"><i class="mdi mdi-window-close"></i> 删除</btn>
                </div>
              </div>
              <div class="card-body">

                <div class="table-responsive">
                  <table class="table table-bordered">
                    <thead>
                    <tr>
                      <th>
                        <label class="lyear-checkbox checkbox-primary">
                          <input type="checkbox" id="check-all"><span></span>
                        </label>
                      </th>
                      <th>商品名</th>
                      <th>商品规格</th>
                      <th>商品作用简介</th>
                      <th>图片</th>
                      <th>操作</th>
                    </tr>
                    </thead>
                    <tbody id="Specifications_tbody">
                    </tbody>
                  </table>
                </div>
                <ul class="pagination" id="Specifications_pageInfo_area">
                </ul>

              </div>
            </div>
          </div>

        </div>

      </div>

    </main>
    <!--End 页面主要内容-->
  </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/static/js/Initpolish.js"></script>
<script>
  var totalRecord,currentPage;
  //导入
  $(function () {
    to_page(1);
  });
  function to_page(pn){
    $.ajax({
      url:"${pageContext.request.contextPath}/specification/findSpecificationsAll",
      data:"pn="+pn,
      type:"Get",
      success:function (result) {
        // let parse = JSON.parse(result);
        builld_Veterinarian_table(result);
        builld_Veterinarian_pageInfo_area(result);
      }
    })
  }
  //构建表格数据
  function builld_Veterinarian_table(result){
    let html="";
    let  specifications=result.extend.PageInfo.list;
    let checkBoxId="SpecificationTableCheck";
    let deleteId="delete";
    let changeId="change";
    $.each(specifications,function (index,item) {
      checkBoxId=checkBoxId+item.drname;
      deleteId=deleteId+item.drname;
      changeId=changeId+item.drname;
      html=html+
          "<tr>"
          +"<td>"+"<label class='lyear-checkbox checkbox-primary'>"+"<input type='checkbox' name='ids' value='"+index+"'  class='check_item' id='"+checkBoxId+"'>"+"<span>"+"</span>"+"</label>"+"</td>"
          +"<td id='drname"+item.drname+"' >"+item.drname+"</td>"
          +"<td>"+item.drspecifications+"</td>"
          +"<td>"+item.drintroduction+"</td>"
          +"<td>"+"<img src='"+item.dimgurl+"' style='height: 64px;width: 64px'>"+"</td>"
          +"<td>"
          +"<div class='btn btn-group'>"+
          "<a class='btn btn-xs btn-default editSpecifications_Moralbtn' id='"+changeId+"' title='编辑' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-pencil'></i>"+"</a>"+
          "<a class='btn btn-xs btn-default deleteSpecifications_Moralbtn' id='"+deleteId+"' title='删除' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-window-close'></i>"+"</a>"+
          "</div>"
          +"</td>"+
          +"</tr>"
      checkBoxId="SpecificationTableCheck";
      deleteId="delete";
      changeId="change";
    })
    $('#Specifications_tbody').html(html);
  }
  //构建导航栏
  function builld_Veterinarian_pageInfo_area(result) {
    let html="";
    //判断还有没有上一页
    if(result.extend.PageInfo.hasPreviousPage==false){
      html=html+"<li class='disabled'  >"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
    }else {
      html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum-1)+")'>"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
    }
    //遍历得到页码号
    $.each(result.extend.PageInfo.navigatepageNums,function (index,item) {
      //判断是不是当前页
      if(result.extend.PageInfo.pageNum==item){
        html=html+"<li class='active' onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
      }else{
        html=html+"<li onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
      }
    });
    //判断还有没有下一页
    if (result.extend.PageInfo.hasNextPage==false){
      html=html+"<li class='disabled' >"+"<a href='#!'>"+">>"+"</a>"+"</li>";
    }else {
      html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum+1)+")'>"+"<a href='#!'>"+">>"+"</a>"+"</li>";
    }
    $('#Specifications_pageInfo_area').html(html);
    totalRecord=result.extend.PageInfo.total;
    currentPage=result.extend.PageInfo.pageNum;
  }
  //给按钮绑定单击事件
  window.onload=function (){
    $('#searchSpecificationsByName-btn').click(function () {
      let elementById = document.getElementById("searchSpecificationsByName_field");
      let name = elementById.value;
      to_page_ByName(name);
    })
  }
  //按姓名条件查询
  function to_page_ByName(name) {
    $.ajax({
      url:"${pageContext.request.contextPath}/specification/findSpecificationsByNames",
      data: "name="+name,
      success:function (result) {
        builld_Veterinarian_table(result);
        builld_Veterinarian_pageInfo_area(result);
      }
    })
  }
  //新增药品规格
  $('#ProductSpecification_add_moral_btn').click(function () {
    let  specificationdImg = document.getElementById("specificationdImg_add_input");
    let formData = new FormData();
    formData.append("drname",$('#specificationDrname_add_input').val());
    formData.append("drspecifications",$('#specificationDrspecifications_add_input').val());
    formData.append("drfunction",$('#specificationDrfunction_add_input').val());
    formData.append("drintroduction",$('#specificationDrfunction_add_input').val());
    formData.append('file',specificationdImg.files[0]);
    $.ajax({
      async: false,
      url:"${pageContext.request.contextPath}/specification/addSpecifications",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success:function (result) {
         document.getElementById("specification_add_form_moral").reset();
         document.getElementById("specification_add_Img").src=null;
         $('#ProductSpecification_add_modal').modal('hide')
      }
    })
  })
  //新增上传图片,获取上传图片input并在img里面修改图片
  $(function (){
    getImgInput("specificationdImg_add_input","specification_add_Img");
    getImgInput("specificationdImg_change_input","specification_change_Img");
  })
  function getImgInput(imgInputId,imgId) {
    const imgInput = document.getElementById(imgInputId);
    imgInput.addEventListener("change",(event)=>{
      let file = imgInput.files[0];
      previewWithObjectURL(file,imgId)
      //箭头函数中imgId，自动向外查找
    })
  }
  //简单的图片预览建议使用第一种方式
  //URL方式（第一种方式）src = blob:http://localhost:8080/f720711f-57e2-428f-8a47-ec59e5dbbc10
  function previewWithObjectURL(file,imgId) {
    var url = URL.createObjectURL(file);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    //请求完成后,显示图片
    reader.onloadend = function (event) {
      document.getElementById(imgId).src = url;
    }
  }
  //给表格中的编辑连接绑定事件
  $(document).on("click",".editSpecifications_Moralbtn",function () {
    // vid 获取此项的id
    let  drname = $(this).attr("id");
    drname  = drname.toString().replace("change","");
    getEmp(drname)
    //将vid放在修改按钮的属性上,这样可以让后端通过vid查找要修改医师
    $('#ProductSpecification_change_moral_btn').attr("specification-drname",drname)
    $('#ProductSpecification_change_modal').modal({
      backdrop:"static"
    })
  })
  //给表格中的删除链接绑定事件
  $(document).on("click",".deleteSpecifications_Moralbtn",function () {
    let drname = $(this).attr("id");
    drname=drname.toString().replace("delete","");
    $.ajax({
      url:"${pageContext.request.contextPath}/specification/deleteSpecificationsByName",
      data:"name="+drname,
      success:function (result) {
        console.log(result);
        to_page(currentPage);
      }
    })
  })
  //拿到点击后表格中id的值,使用Ajax查询信息显示在模态框中
  function getEmp(drname) {
    $.ajax({
      url:"${pageContext.request.contextPath}/specification/findSpecificationsByNamesAccurate",
      data:"drname="+drname,
      success:function (result) {
        let specification=result.extend.specification;
        $('#specificationDrname_change_input').val(specification.drname);
        $('#specificationDrspecifications_change_input').val(specification.drspecifications);
        $('#specificationDrfunction_change_input').val(specification.drfunction);
        $('#specificationDrintroduction_change_input').val(specification.drintroduction);
        $('#specification_change_Img').attr('src',specification.dimgurl)
      }
    })
  }
  //获取模态框中的数据发送ajax请求,修改规格
  $('#ProductSpecification_change_moral_btn').click(function () {
    let  specificationdImg = document.getElementById("specificationdImg_change_input");
    let formData = new FormData();
    formData.append("drname",$('#specificationDrname_change_input').val());
    formData.append("drspecifications",$('#specificationDrspecifications_change_input').val());
    formData.append("drfunction",$('#specificationDrfunction_change_input').val());
    formData.append("drintroduction",$('#specificationDrintroduction_change_input').val());
    if(typeof(specificationdImg.files[0])=="undefined"){//图片未更换
      console.log("1");
    }else {//图片被更换过
      console.log("2");
      formData.append("file",specificationdImg.files[0]);
    }
    $.ajax({
      async: false,
      url:"${pageContext.request.contextPath}/specification/updateSpecificationsByname",
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success:function (result) {
        console.log(result);
        $('#ProductSpecification_change_modal').modal('hide');
        to_page(currentPage);
      }
    })
  })
  //给删除按钮绑定事件,批量删除
  $('#AllSpecification_delete').click(function () {
    let names="";
    let names1="";
    $.each($(".check_item:checked"),function () {
      let name =$(this).attr("id");
      name=name.toString().replace("SpecificationTableCheck","");
      names+=name+",";
      names1+=name+"-";
    })
    names=names.substring(0,names.length-1);
    names1=names1.substring(0,names1.length-1);
    if(confirm("确认删除商品为["+names+"]的商品吗?")){
      $.ajax({
        url:"${pageContext.request.contextPath}/specification/deleteSpecificationsByNames",
        data:"names="+names1,
        success:function (result) {
          console.log(result);
          to_page(currentPage);
        }
      })
    }
  })
  function deleteVeterinarians() {

  }
</script>
</body>
</html>
