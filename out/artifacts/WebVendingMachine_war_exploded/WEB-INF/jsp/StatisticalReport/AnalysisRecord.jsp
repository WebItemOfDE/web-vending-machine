<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>分析记录</title>
    <link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
    <meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
    <meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
    <meta name="author" content="yinqi">
    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>

<body>
<div class="lyear-layout-web">
    <div class="lyear-layout-container">
        <!--左侧导航-->
        <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
        <!--End 左侧导航-->

        <!--头部信息-->
        <header class="lyear-layout-header">

            <nav class="navbar navbar-default">
                <div class="topbar">

                    <div class="topbar-left">
                        <div class="lyear-aside-toggler">
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                        </div>
                        <span class="navbar-page-title"> 后台首页 </span>
                    </div>
                    <%@include file="/pages/static/jsp/rightNavigation.jsp"%>
                </div>
            </nav>

        </header>
        <!--End 头部信息-->

        <!--页面主要内容-->
        <main class="lyear-layout-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>TOP销售商品</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-bars2"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>TOP销售区域</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-lines2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>数据提示</h4>
                            </div>
                            <div class="card-body">


                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <span>3号机器的药品种类减少，增加口罩数量</span>
                                    <br>
                                    <span>提示时间:2021年5月13日19:01:55</span>
                                </div>
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <span>1号药柜的口罩平均售卖50只/日,减少口罩存放数</span>
                                    <br>
                                    <span>提示时间:2021年5月13日19:02:54</span>
                                </div>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <span>5号药水和药品的数量供给应增加</span>
                                    <br>
                                    <span>提示时间：2021年5月13日19:02:58</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--End 页面主要内容-->
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/Chart.js"></script>

<script type="text/javascript">
    'use strict'
     let labels1 = [], data1=[];
    $(document).ready(function (e) {
         $.ajax({
            url:"${pageContext.request.contextPath}/chart/AnalysisRecord",
            type:"Get",
            success:function (result) {
                console.log(result);
                var charts = result.extend.SaleCharts;
                // 将获取到的json数据分别存放到两个数组中
                for(let chartrecord in charts){
                    labels1.push(charts[chartrecord].dname);
                    data1.push(charts[chartrecord].sale_count);
                }

                    var $dashChartBarsCnt2  = jQuery( '.js-chartjs-bars2' )[0].getContext( '2d' ),
            $dashChartLinesCnt2 = jQuery( '.js-chartjs-lines2' )[0].getContext( '2d' );

        var $dashChartBarsData2 = {
            labels: labels1,
            datasets: [
                {
                    label: 'TOP销售商品',
                    borderWidth: 1,
                    borderColor: 'rgba(0,0,0,0)',
                    backgroundColor: 'rgba(51,202,185,0.5)',
                    hoverBackgroundColor: "rgba(51,202,185,0.7)",
                    hoverBorderColor: "rgba(0,0,0,0)",
                    data: data1
                }
            ]
        };
        var $dashChartLinesData2 = {
            labels: ['郑州二七区', '郑州中原区', '郑州金水区', '郑州上街区', '郑州惠济区', '郑州回族管城区','郑州高新区'],
            datasets: [
                {
                    label: 'TOP销售区域',
                    data: [23, 25, 30, 40, 45, 48, 55],
                    borderColor: '#358ed7',
                    backgroundColor: 'rgba(53, 142, 215, 0.175)',
                    borderWidth: 1,
                    fill: false,
                    lineTension: 0.5
                }
            ]
        };
        new Chart($dashChartBarsCnt2, {
            type: 'bar',
            data: $dashChartBarsData2
        });
        new Chart($dashChartLinesCnt2, {
            type: 'bar',
            data: $dashChartLinesData2,
        });

           }
        })
    })

    $(function(){
        $('.search-bar .dropdown-menu a').click(function() {
            var field = $(this).data('field') || '';
            $('#search-field').val(field);
            $('#search-btn').html($(this).text() + ' <span class="caret"></span>');
        });
    });

    $(document).ready(function(e) {
        // 使用时，请自行引入jquery.cookie.js
        // 读取cookie中的主题设置
        var the_logo_bg    = $.cookie('the_logo_bg'),
            the_header_bg  = $.cookie('the_header_bg'),
            the_sidebar_bg = $.cookie('the_sidebar_bg'), // iframe版本如果删除了下面一行，请把这一行的逗号改成分号
            the_site_theme = $.cookie('the_site_theme'); // iframe版本可不需要这行

        if (the_logo_bg) $('body').attr('data-logobg', the_logo_bg);
        if (the_header_bg) $('body').attr('data-headerbg', the_header_bg);
        if (the_sidebar_bg) $('body').attr('data-sidebarbg', the_sidebar_bg);
        if (the_site_theme) $('body').attr('data-theme', the_site_theme); // iframe版本可不需要这行

        // 处理主题配色下拉选中
        $(".dropdown-skin :radio").each(function(){
            var $this = $(this),
                radioName = $this.attr('name');
            switch (radioName) {
                case 'site_theme':
                    $this.val() == the_site_theme && $this.prop("checked", true);
                    break;  // iframe版中不需要这个case
                case 'logo_bg':
                    $this.val() == the_logo_bg && $this.prop("checked", true);
                    break;
                case 'header_bg':
                    $this.val() == the_header_bg && $this.prop("checked", true);
                    break;
                case 'sidebar_bg':
                    $this.val() == the_sidebar_bg && $this.prop("checked", true);
            }
        });

        // 设置主题配色
        setTheme = function(input_name, data_name) {
            $("input[name='"+input_name+"']").click(function(){
                $('body').attr(data_name, $(this).val());
                $.cookie('the_'+input_name, $(this).val());
            });
        }
        setTheme('site_theme', 'data-theme'); // iframe版本可不需要这行
        setTheme('logo_bg', 'data-logobg');
        setTheme('header_bg', 'data-headerbg');
        setTheme('sidebar_bg', 'data-sidebarbg');
    });
</script>

</body>
</html>