<%--
  Created by IntelliJ IDEA.
  User: 86130
  Date: 2021/8/2
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
</head>
<body>
  <form method="post" enctype="multipart/form-data">
    选择要上传的文件：<br/>
    <input type="file" name="file" id="myFile"/><span></span>
  </form>
  <div id="bar">
    上传进度:
    <progress id="pro" value="0"></progress>
  </div>
  <input type="button" id="upLoadSub" value="上传"/>
  <div>
    <img src="" alt="GG" id="myImg" width="200" height="160"/>
  </div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script>
    var myFile = document.getElementById("myFile");
    //绑定事件(ECMAScript6写法)
    myFile.addEventListener("change", () => {
    var file = myFile.files[0];
    //预览图片函数
    previewWithObjectURL(file);
  })
    //点击->图片上传   //axios轻量级 ajax API
    $("#upLoadSub").on("click", function () {
    var myFile = document.getElementById("myFile");
    var formData = new FormData();
    formData.append("file", myFile.files[0]);
    axios({
    method: 'post',
    url: '/addQiniu',
    data: formData
  }).then(function (response) {
    console.log(response.data);
  }).catch(console.error);
  });


    //简单的图片预览建议使用第一种方式
    //URL方式（第一种方式）src = blob:http://localhost:8080/f720711f-57e2-428f-8a47-ec59e5dbbc10
    function previewWithObjectURL(file) {
    var url = URL.createObjectURL(file);
    var reader = new FileReader();
    reader.readAsDataURL(file);
    //请求完成后,显示图片
    reader.onloadend = function (event) {
    document.getElementById("myImg").src = url;
  }
    //上传过程中动态显示进度条
    reader.onprogress = function (event) {
    if (event.lengthComputable) {
    document.getElementById("pro").value = event.loaded / event.total;
  }
  }
  }
</script>
</html>
