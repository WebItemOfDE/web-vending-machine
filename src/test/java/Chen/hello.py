import sys

class Hello:
    def __init__(self,name,age):
        self.name = name
        self.age = age

    def hello(self):
        print("name:",self.name,"\n","age:",self.age,"\n")



if __name__ == "__main__":
    re = Hello(sys.argv[1],sys.argv[2])
    re.hello()