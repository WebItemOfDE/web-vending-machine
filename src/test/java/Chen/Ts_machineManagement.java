package Chen;

import com.t.pojo.Machine;
import com.t.service.machineManagementService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class Ts_machineManagement {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

    @Test
    public void TsaddMachine(){
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);

        List<Machine> machines = new ArrayList<>();

        for (int i = 4; i < 9; i++) {
            Machine machine = new Machine(String.valueOf(i),"测试机"+i,"科学大道"+i,"测试"+i);
            machines.add(machine);
        }
        for (Machine machine : machines) {
            service.addMachine(machine);

        }

    }
    @Test
    public void TsupdateMachine() {
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);
        Machine machine = new Machine("2","测试机ss","科学大ss道","测试");
        int i = service.updateMachine(machine);
        System.out.println(i);
    }
    @Test
    public void TsdeleteMachine(){
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);
        Machine machine = new Machine("3","测试机ss","科学大ss道","测试");
        int i = service.deleteMachine(machine);
        System.out.println(i);

    }
    @Test
    public void TsdeleteMachines(){
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);
        List<Machine> machines = new ArrayList<>();
        for (int i = 4; i < 9; i++) {
            Machine machine = new Machine(String.valueOf(i),"测试机"+i,"科学大道"+i,"测试"+i);
            machines.add(machine);
        }
        int i = service.deleteMachines(machines);
        System.out.println(i);

    }
    @Test
    public void TsfindAll(){
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);
        List<Machine> all = service.findAll();
        for (Machine machine : all) {
            System.out.println(machine.toString());
        }
    }
    @Test
    public void TsfindByName(){
        machineManagementService service = context.getBean("machineManagementServiceImpl", machineManagementService.class);
        List<Machine> byName = service.findByName("1");

    }

}
