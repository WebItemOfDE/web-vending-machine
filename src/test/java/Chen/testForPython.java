package Chen;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.Buffer;

public class testForPython {

    @Test
    public void testone(){
        String[] arguments = new String[]{
                "python",
                "src/main/resources/PythonFile/testclient.py",
               "1"
        };
        try {
            Process process = Runtime.getRuntime().exec(arguments);
            BufferedReader  in = new BufferedReader(new InputStreamReader(process.getInputStream(),"GBK"));
            String line =null;
            while ((line = in.readLine())!= null){
                System.out.println(line);
            }
            in.close();
            //java代码中的process.waitFor()返回值为0表示我们调用python脚本成功，
            //返回值为1表示调用python脚本失败，这和我们通常意义上见到的0与1定义正好相反
            int re =process.waitFor();
            System.out.println(re);


        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
