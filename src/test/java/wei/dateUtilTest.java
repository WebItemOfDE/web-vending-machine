package wei;

import com.t.service.saleService;
import com.t.util.ToolsMapper;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.util.Date;

public class dateUtilTest {
    @Test
    public void test1() throws ParseException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        ToolsMapper mapper = context.getBean("toolsMapper", ToolsMapper.class);
        String  dateStr="2021-08-07 10:53:33";
        Date date = mapper.stringToDate(dateStr);
        System.out.println(date.toString());
    }
    @Test
    public void test2() throws ParseException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        ToolsMapper mapper = context.getBean("toolsMapper", ToolsMapper.class);
        String  dateStr="2021-08-07";
        Date date = mapper.stringToDateSimple(dateStr);
        System.out.println(date.toString());
    }
}
