import com.t.pojo.*;
import com.t.service.*;
import com.t.util.ToolsMapper;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.util.*;

public class test {
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    @Test
    public void test(){
        System.out.println(123);
    }
    @Test
    public void drugTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        List<Drug> Drugs = service.findDrugAll();
        for (Drug drug : Drugs) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public void userLoginTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        userService service = (userService) context.getBean("userServiceImpl");
        User user = service.loginCheck(new User("123", "123"));
        System.out.println("用户检查结果为:"+user);
    }
    @Test
    public void userSelect(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        userService service = (userService) context.getBean("userServiceImpl");
        User user = service.SelectUserByAccount("123");
        System.out.println(user);
    }
    @Test
    public void userSelectMapperTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        userService service = (userService) context.getBean("userServiceImpl");
        List<User> userAll = service.findUserAll();
        for (User user : userAll) {
            System.out.println(user.toString());
        }
    }
    @Test
    public void drugAddTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        service.addDrug(new Drug("2","999", 12.5F,20,new Date(),new Date(),null,"0",null));
    }
    @Test
    public void drugUpdateTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        Drug drug = new Drug();
        drug.setDrid("1");
        drug.setDrname("9999感冒灵");
        service.updateDrug(drug);
    }
    @Test
    public void drugDeleteTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        service.deleteDrugById("1");
    }
    @Test
    public void drugDeleteByIdsTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        List<Integer> list=new ArrayList<Integer>();
        list.add(121);
        list.add(123);
        int i = service.deleteDrugByIds(list);
        System.out.println(i);
    }
    @Test
    public void drugSelectByConditionTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        HashMap<String, String> map = new HashMap<>();
        map.put("drname","");
        List<Drug> list = service.findDrugByCondition(map);
        for (Drug drug : list) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public  void drugSelectByDridTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        ArrayList<Integer> drugId = new ArrayList<>();
        drugId.add(Integer.valueOf(1));
        List<Drug> drugs = service.findDrugByIds(drugId);
        for (Drug drug : drugs) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public void drugFindByMachineIdTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        ArrayList list=new ArrayList();
        list.add(2);
        List<Drug> drugs = service.findDrugByMachineId(list);
        for (Drug drug : drugs) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public void drugFindByMachineId1Test(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        List<Drug> drugs = service.findDrugByMachineId1(1);
        for (Drug drug : drugs) {
            System.out.println(drug);
        }
    }
    @Test
    public  void drugSelectByDridTest1(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        ArrayList drugId = new ArrayList();
        HashMap<String, String> map = new HashMap<>();
        map.put("drid","1");
        drugId.add(map);
        List<Drug> drugs = service.findDrugByIds(drugId);
        for (Drug drug : drugs) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public void findDrugImgByMachineId(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        List<Drug> drugList = service.findDrugImgByMachineId("1");
        for (Drug drug : drugList) {
            System.out.println(drug);
        }
    }
    @Test
    public void machineSelectTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        machineManagementService service = (machineManagementService) context.getBean("machineServiceImpl");
        List<Machine> machines = service.findAll();
        for (Machine machine : machines) {
            System.out.println(machine.toString());
        }
    }
//    @Test
//    public  void    machineInsertTest(){
//        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
//        machineService service = (machineService) context.getBean("machineServiceImpl");
//        Drug drug = new Drug("2", "999", 12.5F, 20, new Date(), new Date(), "www.baidu.com");
//        ArrayList<Drug> drugs = new ArrayList<Drug>();
//        drugs.add(drug);
//        service.insertMachine(new Machine("2","东风路2号机器","东风路",drugs));
//    }
    @Test
    public void toolsMppaerTest(){
        ToolsMapper t=new ToolsMapper();
        System.out.println(t.CreateId());
    }
    @Test
    public void findSpecificationsAll(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        service.findSpecificationsAll();
    }
    @Test
    public void findSpecificationsByname(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        List<Specifications> specifications = service.findSpecificationsByNames("9");
        System.out.println(specifications);
    }
    @Test
    public void addSpecifications(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        Specifications specifications1 = new Specifications("1", "1", "1", "1", "1");
        service.addSpecifications(specifications1);
    }
    @Test
    public void findSpecificationsByNamesAccurate(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        System.out.println(service.findSpecificationsByNamesAccurate("医用口罩"));
    }
    @Test
    public void  deleteSpecificationsByName(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        int i = service.deleteSpecificationsByName("1");
        System.out.println(i);
    }
    @Test
    public void deleteSpecificationsByNames(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SpecificationService  service = (SpecificationService)context.getBean("specificationServiceImpl");
        List<String> list=new ArrayList<>();
        list.add("1");
        list.add("2");
        service.deleteSpecificationsByNames(list);
    }
    @Test
    public void saleAddTest() throws ParseException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        saleService service = context.getBean("saleServiceImpl",saleService.class);
        ToolsMapper mapper = context.getBean("toolsMapper", ToolsMapper.class);
        String  dateStr="2021-08-07 10:53:33";
        Sale sale = new Sale(mapper.CreateId(), "医用口罩", "1", 20, 1, mapper.stringToDate(dateStr), "156");
        service.addSale(sale);
    }
    @Test
    public void findSaleAll(){
        saleService service = context.getBean("saleServiceImpl",saleService.class);
        System.out.println(service.findSaleAll().toString());
    }
    @Test
    public void findSaleByNameDate() throws ParseException {
        saleService service = context.getBean("saleServiceImpl",saleService.class);
        ToolsMapper mapper = context.getBean("toolsMapper", ToolsMapper.class);
        String dname="医用";
        Date date = mapper.stringToDate("2021-08-13 15:24:02");
        Date date1 = mapper.stringToDate("2021-08-15 10:53:33");
        HashMap map=new HashMap();
        map.put("dname",dname);
        map.put("startDate",date);
        map.put("endDate",date1);
        service.findSaleByNameDate(map);
    }
    @Test
    public void splitStringTest(){
        String str="nihao";
        String[] split = str.split("-");
        for (String s : split) {
            System.out.println(s);
        }
    }
    @Test
    public void deleteSaleByNums(){
        ArrayList<String> list = new ArrayList<>();
        list.add("cw30519367");
        saleService service = context.getBean("saleServiceImpl",saleService.class);
        service.deleteSaleByNums(list);
    }
    @Test
    public void findDrugImgByMachineDrugname(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        HashMap<String, String> map = new HashMap<>();
        map.put("drname","9");
        map.put("mhid","2");
        List<Drug> list = service.findDrugImgByMachineDrugname(map);
        for (Drug drug : list) {
            System.out.println(drug.toString());
        }
    }
    @Test
    public void deleteDrugByIdsMachine(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        HashMap map = new HashMap();
        ArrayList ids = new ArrayList();
        ids.add("1");
        map.put("ids",ids);
        map.put("mhid","1");
        service.deleteDrugByIdsMachine(map);
    }
    @Test
    public void findDrugImgByMachineDrugnameAccurate(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        drugService service = (drugService) context.getBean("drugServiceImpl");
        HashMap<String, String> map = new HashMap<>();
        map.put("drname","999感冒");
        map.put("mhid","2");
        Drug drug = service.findDrugImgByMachineDrugnameAccurate(map);
        System.out.println(drug);
    }
}
