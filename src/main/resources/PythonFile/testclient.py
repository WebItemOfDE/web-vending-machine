# coding: utf-8
import socket
import sys


class Client:
    def __init__(self,data,HOST='192.168.43.128' ,PORT = 50007,BUFSIZ = 1024):
        self.HOST = HOST    # 服务器的ip
        self.PORT = PORT              # 需要连接的服务器的端口
        self.BUFSIZ = 1024
        self.ADDR = (HOST,PORT)
        self.data = data
        print('我是客户端！')

    def act(self):
        c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        c.connect(self.ADDR)
        while 1:
            # data=input("请输入:\n >>>")
            if not self.data:
                break
            c.sendall(self.data.encode())  # 发送信息给服务器
            data = c.recv(self.BUFSIZ).decode()
            if not data:
                break
            print ('接收到 >>>', repr(data))  # 打印从服务器接收回来的数据
        c.close()


if __name__ == "__main__":
    way = Client(sys.argv[1])
    way.act()