package com.t.util;


import java.util.Date;
import java.text.ParseException;

public interface Tools {
    String CreateId();
    Date stringToDate(String dateStr) throws ParseException;
    Date stringToDateSimple(String dateStr) throws ParseException;
}
