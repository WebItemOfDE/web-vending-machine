package com.t.util;

import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Component
public class ToolsMapper implements Tools{
    @Override
    public String CreateId() {
        // 8位
        String res = "cw";
        Random r = new Random();

        for (int i = 0; i < 8; i++) {
           res += r.nextInt(10);
        }

        return res;
    }

    @Override
    public Date stringToDate(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = sdf.parse(dateStr);
        return date;
    }

    @Override
    public Date stringToDateSimple(String dateStr) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dateStr);
        return date;
    }
}
