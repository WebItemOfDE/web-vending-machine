package com.t.controller;

import com.t.pojo.User;
import com.t.service.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/user")
public class userController {
    @Autowired
    @Qualifier("userServiceImpl")
    private userService userService;

    @RequestMapping("/checkUser")    //跳转成功
    public String checkUser(String userAccount, String userPassword, Model model, HttpServletRequest request){
        System.out.println("接收到请求");
        User user = new User(userAccount, userPassword);
        User user1 = userService.loginCheck(user);
        if(user1!=null){ //查到了
            HttpSession session = request.getSession();
            session.setAttribute("account1",userAccount);
            model.addAttribute("account",userAccount);
            return "index";
        }else{ //没查到
          model.addAttribute("error","密码或用户名错误")  ;
          return "login"  ;
        }
    }
    @RequestMapping("/toLogin")  //跳转成功
    public String toLogin(){
        return "login";
    }
    @RequestMapping("/toIndex")
    public String toIndex(){
        return "index";
    }
}
