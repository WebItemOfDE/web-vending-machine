package com.t.controller;

import com.t.pojo.Machine;
import com.t.pojo.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

@Controller
@RequestMapping("/wxMachine")
public class WechatMachineController {
    @Autowired
    @Qualifier("machineServiceImpl")
    private com.t.service.machineManagementService machineManagementService;

    @RequestMapping("/AllMachine")
    @ResponseBody
    public Msg AllMachine(String  machineId){
        System.out.println("machineId>>"+machineId);
//        id
        System.out.println("开始调动Python");
        String[] arguments = new String[]{
                "python",
                "D:\\IdeaProjects\\maven_test1\\WebVendingMachine\\src\\main\\resources\\PythonFile\\testclient.py",
                machineId
        };
        int re = 0;
        try {
            Process process = Runtime.getRuntime().exec(arguments);
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream(),"GBK"));
            String line =null;
            while ((line = in.readLine())!= null){
                System.out.println(line);
            }
            in.close();
            //java代码中的process.waitFor()返回值为0表示我们调用python脚本成功，
            //返回值为1表示调用python脚本失败，这和我们通常意义上见到的0与1定义正好相反
            re =process.waitFor();
            System.out.println("返回结果"+re);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

//        List<Machine> machines = machineService.findMachineAll();
        return Msg.success().add("AllMachine",re);
    }
    @ResponseBody
    @RequestMapping("/findAll")
    public Msg findAll(){
        List<Machine> machines = machineManagementService.findAll();
        return  Msg.success().add("machine",machines);
    }
}
