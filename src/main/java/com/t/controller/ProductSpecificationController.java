package com.t.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.t.pojo.Msg;
import com.t.pojo.Specifications;
import com.t.service.SpecificationService;
import com.t.util.QiniuCloud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/specification")
public class ProductSpecificationController {
    @Qualifier("specificationServiceImpl")
    @Autowired
    SpecificationService service;
    @RequestMapping("/toProductSpecification")
    public String toProductSpecification(){
        return "EditProducts/ProductSpecification";
    }

    @ResponseBody
    @RequestMapping("/findSpecificationsAll")
    public Msg findSpecificationsAll(@RequestParam(value = "pn",defaultValue = "1")Integer pn){
        PageHelper.startPage(pn,5);
        List<Specifications> specifications = service.findSpecificationsAll();
        PageInfo page = new PageInfo(specifications,5);
        return Msg.success().add("PageInfo",page);
    }
    @ResponseBody
    @RequestMapping("/findSpecificationsByNames")
    public Msg findSpecificationsByNames(@RequestParam(value = "name",defaultValue = "")String name){
        PageHelper.startPage(1,50);
        List<Specifications> specifications = service.findSpecificationsByNames(name);
        PageInfo page = new PageInfo(specifications,5);
        return Msg.success().add("PageInfo",page);
    }
    //添加药品规格
    @ResponseBody
    @RequestMapping(value = "/addSpecifications",method = RequestMethod.POST)
    public Msg addSpecifications(@RequestParam("drname")String drname,@RequestParam("drspecifications")String drspecifications,@RequestParam("drfunction")String drfunction,@RequestParam("drintroduction")String drintroduction,@RequestParam("file")CommonsMultipartFile file,HttpServletRequest request) throws IOException {
        Specifications specifications = new Specifications(drname, drspecifications, drfunction, drintroduction);
        String uploadFileName=file.getOriginalFilename();
        String path = request.getServletContext().getRealPath("/upload");
        File realPath = new File(path);
        if(!realPath.exists()){
            realPath.mkdir();
        }
        //将文件写出到本地
        InputStream is = file.getInputStream(); //文件输入流
        OutputStream os = new FileOutputStream(new File(realPath,uploadFileName)); //文件输出流
        //将文件读取写出到本地
        int len=0;
        byte[] buffer = new byte[1024];
        while ((len=is.read(buffer))!=-1){
            os.write(buffer,0,len);
            os.flush();
        }
        os.close();
        is.close();
//        拿到上传工具类的对象，获取File的路径
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        QiniuCloud bean = context.getBean("qiniuCloud", QiniuCloud.class);
        String dimgurl = bean.upload(realPath.getPath()+"\\"+uploadFileName, uploadFileName);
        specifications.setDimgurl(dimgurl);
        service.addSpecifications(specifications);
        //上传成功后删除本地文件
        File file1 = new File(realPath.getPath() + "\\" + uploadFileName);
        return Msg.success();
    }
    //报错，处理Get请求
    @ResponseBody
    @RequestMapping(value = "/addSpecifications",method = RequestMethod.GET)
    public Msg addSpecifications1(@RequestParam("drname")String drname,@RequestParam("drspecifications")String drspecifications,@RequestParam("drfunction")String drfunction,@RequestParam("drintroduction")String drintroduction,@RequestParam("file")CommonsMultipartFile file){
        Specifications specifications = new Specifications(drname, drspecifications, drfunction, drintroduction);
        System.out.println(specifications.toString());
        String uploadFileName=file.getOriginalFilename();
        System.out.println("文件名"+uploadFileName);
        return Msg.success();
    }
    //根据要药品名获取规格
    @ResponseBody
    @RequestMapping("/findSpecificationsByNamesAccurate")
    public Msg findSpecificationsByNamesAccurate(@RequestParam("drname")String drname){
        Specifications specification = service.findSpecificationsByNamesAccurate(drname);
        return Msg.success().add("specification",specification);
    }
    @ResponseBody
    @RequestMapping(value = "/updateSpecificationsByname",method = RequestMethod.POST)
    public Msg updateSpecificationsByname(@RequestParam("drname")String drname,@RequestParam("drspecifications")String drspecifications,@RequestParam("drfunction")String drfunction,@RequestParam("drintroduction")String drintroduction,@RequestParam(value = "file",required = false)CommonsMultipartFile file,HttpServletRequest request) throws IOException {
        Specifications specifications = new Specifications(drname, drspecifications, drfunction, drintroduction);
        System.out.println(specifications.toString());
        if(file!=null){
            String uploadFileName=file.getOriginalFilename();
            String path = request.getServletContext().getRealPath("/upload");
            File realPath = new File(path);
            if(!realPath.exists()){
                realPath.mkdir();
            }
            //将文件写出到本地
            InputStream is = file.getInputStream(); //文件输入流
            OutputStream os = new FileOutputStream(new File(realPath,uploadFileName)); //文件输出流
            //将文件读取写出到本地
            int len=0;
            byte[] buffer = new byte[1024];
            while ((len=is.read(buffer))!=-1){
                os.write(buffer,0,len);
                os.flush();
            }
            os.close();
            is.close();
//        拿到上传工具类的对象，获取File的路径
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
            QiniuCloud bean = context.getBean("qiniuCloud", QiniuCloud.class);
            String dimgurl = bean.upload(realPath.getPath()+"\\"+uploadFileName, uploadFileName);
            specifications.setDimgurl(dimgurl);
            //上传成功后删除本地文件
            File file1 = new File(realPath.getPath() + "\\" + uploadFileName);
        }
        service.updateSpecificationsByname(specifications);
        return Msg.success();
    }
    //根据商品名删除规格
    @ResponseBody
    @RequestMapping("/deleteSpecificationsByName")
    public Msg deleteSpecificationsByName(@RequestParam("name")String name){
        service.deleteSpecificationsByName(name);
        return Msg.success();
    }
    //根据姓名批量进行删除
    @ResponseBody
    @RequestMapping("/deleteSpecificationsByNames")
    public Msg deleteSpecificationsByNames(@RequestParam("names")String names){
        String[] split = names.split("-");
        List<String> list=new ArrayList<>();
        for (String s : split) {
            list.add(s);
        }
        service.deleteSpecificationsByNames(list);
        return Msg.success();
    }
}
