package com.t.controller;

import com.t.pojo.Msg;
import com.t.pojo.Sale;
import com.t.service.saleService;
import com.t.util.ToolsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.Date;

@Controller
@ResponseBody
@RequestMapping("/wxSale")
public class wechatSaleController {
    @Autowired
    @Qualifier("saleServiceImpl")
    saleService service;
    @RequestMapping(value = "/addSale",method = RequestMethod.POST)
    public Msg addSale(String dname,String mhid, Double salePrice,Integer saleNumber,String saleDate,String saleUseropenid) throws ParseException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        ToolsMapper mapper = context.getBean("toolsMapper", ToolsMapper.class);
        service.addSale(new Sale(mapper.CreateId(), dname, mhid, salePrice, saleNumber, mapper.stringToDate(saleDate), saleUseropenid));
        //销售之后减去相应的库存业务没有完成
        return Msg.success();
    }
}
