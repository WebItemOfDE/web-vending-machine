package com.t.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.t.pojo.Drug;
import com.t.pojo.Msg;
import com.t.pojo.SaleChart;
import com.t.service.chartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/chart")
public class ChartController {
    @Autowired
    @Qualifier("chartServiceImpl")
    private chartService chartService;

    //跳转到所有商品界面
    @RequestMapping("/toAnalysisRecord")
    public String toAllProducts(){
        return "StatisticalReport/AnalysisRecord";
    }

    //将请求的页面数据送到指定页面中，并指定传入的页数
    @RequestMapping("/AnalysisRecord")
    @ResponseBody
    public Msg AllProducts(){
        List<SaleChart> charts = chartService.findChartByGroup();
        return Msg.success().add("SaleCharts",charts);
    }
}
