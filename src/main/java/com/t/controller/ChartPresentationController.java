package com.t.controller;

import com.t.dao.chartPresentationMapper;
import com.t.pojo.ChartPresentation;
import com.t.pojo.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
@Controller
@RequestMapping("/presentation")
public class ChartPresentationController {
    @Autowired

    private chartPresentationMapper chartPresentationMapper;

    //跳转到所有商品界面
    @RequestMapping("/toChartPresentation")
    public String toAllProducts() {
        return "StatisticalReport/ChartPresentation";
    }

    //将请求的页面数据送到指定页面中，并指定传入的页数
    @RequestMapping("/ChartPresentation")
    @ResponseBody
    public Msg AllProducts() {
        List<ChartPresentation> charts = chartPresentationMapper.findChartPresentationByGroup();
        return Msg.success().add("ChartPresentation", charts);
    }
}