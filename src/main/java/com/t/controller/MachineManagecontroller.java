package com.t.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.t.pojo.Drug;
import com.t.pojo.Machine;
import com.t.pojo.Msg;
import com.t.service.machineManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/machine")
public class MachineManagecontroller {

    @Autowired
    @Qualifier("machineManagementServiceImpl")
    private machineManagementService service;

    @RequestMapping("/AllMachine")
    @ResponseBody
    public Msg AllMachine(@RequestParam(value = "pn",defaultValue = "1") Integer pn){
        //传入参数为设置从第几页开始查询，和每一页的数据数量
        PageHelper.startPage(pn,5);
        // startPage后面紧跟着的就是分页查询
        List<Machine> machines = service.findAll();
        //使用PageInfo包装查询后的结果，只需要将PageInfo交给页面即可
        //PageInfo里面封装了详细的分页信息，包括我们查出来的数据
        PageInfo page = new PageInfo(machines,5);
//        封装一下返回msg对象 msg中既包含了PageInfo也有我们自定义的状态信息
        return Msg.success().add("PageInfo",page);
    }

    @RequestMapping("/ToManagemnet")
    public String ToManagemnet(){
        return "VendingMachine/MachineManagement";
    }

    //新增机器
    @ResponseBody
    @RequestMapping(value = "/addMachine",method = RequestMethod.POST)
    public Msg addDrug(Machine machine){
        service.addMachine(machine);
        return Msg.success();
    }

    //检验机器编号是否已经存在,若存在不可添加
    @ResponseBody
    @RequestMapping("/checkMachine")
    public Msg checkDrug(@RequestParam("mhid") String mhid){
        ArrayList machineId = new ArrayList();
        HashMap<String, String> map = new HashMap<>();
        map.put("mhid",mhid);
        machineId.add(map);
        List<Machine> machines = service.findMachineByIds(machineId);
        System.out.println(machines.size());
        if(machines.size()>0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
    }

    //根据修改机器id查找信息显示在模态框中
    @ResponseBody
    @RequestMapping("/findMachineByIds")
    public Msg MsgfindMachineByIds(Integer mhid){
        ArrayList<Integer> list=new ArrayList<>();
        list.add(mhid);
        List<Machine> machineByIds = service.findMachineByIds(list);
        return Msg.success().add("machine",machineByIds);
    }

    //修改选择的机器信息
    @ResponseBody
    @RequestMapping("/updateMachine")
    public Msg updateMachine(Machine machine){
        int i = service.updateMachine(machine);
        if(i==0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
    }

    //删除药品
    @ResponseBody
    @RequestMapping("/deleteMachine")
    public Msg deleteDrugById(@RequestParam("mhid")String mhid) {
        int i = service.deleteMachineById(mhid);
        if (i == 0) {
            return Msg.fali();
        } else {
            return Msg.success();
        }
    }

    @ResponseBody
    @RequestMapping("/deleteMachineByIds")
    public Msg deleteMachineByIds(@RequestParam(value ="mhids" )String mhids){
        String[] split = mhids.split("-");
        ArrayList<Integer> list=new ArrayList<>();
        for (String s : split) {
            list.add(Integer.parseInt(s));
        }
        int i = service.deleteMachineByIds(list);
        if(i==0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
    }

    @RequestMapping("/selecProductsByCondition")
    @ResponseBody
//    value = "pn",defaultValue = "1") Integer pn,
    public Msg selecProductsByCondition(@RequestParam Map<String,String> map){
        int pn ;
        if(map.get("pn")==null||map.get("pn").equals("")){
            pn=1;
        }else{
            pn=Integer.valueOf(map.get("pn"));
        }
        PageHelper.startPage(pn,50);
        map.remove("pn");
        List<Machine> machines = service.findMachineByCondition((HashMap<String, String>) map);
        PageInfo page = new PageInfo(machines,5);
        return Msg.success().add("PageInfo",page);
    }

}
