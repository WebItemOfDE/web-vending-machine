package com.t.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.t.pojo.Drug;
import com.t.pojo.Msg;
import com.t.service.drugService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/drug")
public class drugController {
    @Autowired
    @Qualifier("drugServiceImpl")
    private drugService drugService;

    //跳转到所有商品界面
    @RequestMapping("/toAllProducts")
    public String toAllProducts(){
        return "EditProducts/AllProducts";
    }

    //将请求的页面数据送到指定页面中，并指定传入的页数
    @RequestMapping("/AllProducts")
    @ResponseBody
    public Msg AllProducts(@RequestParam(value = "pn",defaultValue = "1") Integer pn){
        //传入参数为设置从第几页开始查询，和每一页的数据数量
        PageHelper.startPage(pn,5);
        // startPage后面紧跟着的就是分页查询
        List<Drug> drugs = drugService.findDrugAll();
        //使用PageInfo包装查询后的结果，只需要将PageInfo交给页面即可
        //PageInfo里面封装了详细的分页信息，包括我们查出来的数据
        PageInfo page = new PageInfo(drugs,5);
//        封装一下返回msg对象 msg中既包含了PageInfo也有我们自定义的状态信息
        return Msg.success().add("PageInfo",page);
    }
    @RequestMapping("/selecProductsByCondition")
    @ResponseBody
//    value = "pn",defaultValue = "1") Integer pn,
    public Msg selecProductsByCondition(@RequestParam Map<String,String> map){
          int pn ;
          if(map.get("pn")==null||map.get("pn").equals("")){
              pn=1;
          }else{
              pn=Integer.valueOf(map.get("pn"));
          }
          PageHelper.startPage(pn,50);
          map.remove("pn");
          List<Drug> drugs = drugService.findDrugByCondition((HashMap<String, String>) map);
          PageInfo page = new PageInfo(drugs,5);
          return Msg.success().add("PageInfo",page);
    }
    //新增药品
    @ResponseBody
    @RequestMapping(value = "/drugOperation",method = RequestMethod.POST)
    public Msg addDrug(Drug drug){
        drugService.addDrug(drug);
        return Msg.success();
    }
    //检验药品编号是否已经存在,若存在不可添加
    @ResponseBody
    @RequestMapping("/checkDrug")
    public Msg checkDrug(@RequestParam("drid") String drid){
        ArrayList drugId = new ArrayList();
        drugId.add(Integer.valueOf(drid));
        List<Drug> drugs = drugService.findDrugByIds(drugId);
        System.out.println(drugs.size());
        if(drugs.size()>0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
    }
    //删除药品
    @ResponseBody
    @RequestMapping("/deleteDrug")
    public Msg deleteDrugById(@RequestParam("drid")String drid) {
        int i = drugService.deleteDrugById(drid);
        if (i == 0) {
            return Msg.fali();
        } else {
            return Msg.success();
        }
    }
    //根据修改药品id查找信息显示在模态框中
    @ResponseBody
    @RequestMapping("/findDrugByIds")
    public Msg MsgfindDrugByIds(Integer drid){
        ArrayList<Integer> list=new ArrayList<>();
        list.add(drid);
        List<Drug> drugByIds = drugService.findDrugByIds(list);
        return Msg.success().add("drug",drugByIds);
    }
   //修改选择的药品信息
   @ResponseBody
   @RequestMapping("/updateDrug")
   public Msg updateDrug(Drug drug){
       int i = drugService.updateDrug(drug);
       if(i==0){
           return Msg.fali();
       }else{
           return Msg.success();
       }
   }
   @ResponseBody
    @RequestMapping("/deleteDrugByIds")
    public Msg deleteDrugByIds(@RequestParam(value ="drids" )String drids){
       String[] split = drids.split("-");
       ArrayList<Integer> list=new ArrayList<>();
       for (String s : split) {
           list.add(Integer.parseInt(s));
       }
       int i = drugService.deleteDrugByIds(list);
        if(i==0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
   }
}
