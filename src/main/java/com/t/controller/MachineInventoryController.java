package com.t.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.t.pojo.Drug;
import com.t.pojo.Msg;
import com.t.pojo.Specifications;
import com.t.service.SpecificationService;
import com.t.service.drugService;
import com.t.util.ToolsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.*;

@Controller
@RequestMapping("/inventory")
public class MachineInventoryController {
    @Autowired
    @Qualifier("drugServiceImpl")
    private drugService drugService;
    @Autowired
    @Qualifier("specificationServiceImpl")
    private SpecificationService specificationService;

    //跳转到所有商品界面
    @RequestMapping("/toAllProducts")
    public String toAllProducts(String mhid, Model model){
        System.out.println(mhid);
        model.addAttribute("mhid",mhid);
        return "VendingMachine/MachineInventory";
    }
    //根据mhid和页数查询机器中相应的药品数据
    @RequestMapping(value = "/AllProducts")
    @ResponseBody
    public Msg AllProducts(@RequestParam(value = "pn",defaultValue = "1") Integer pn,@RequestParam("mhid") String mhid){
        PageHelper.startPage(pn,5);
        List<Drug> drugs = drugService.findDrugImgByMachineId(mhid);
        PageInfo page = new PageInfo(drugs,5);
        return Msg.success().add("PageInfo",page);
    }
    //根据药品名和机器号查询相应的药品
    @ResponseBody
    @RequestMapping("/findDrugImgByMachineDrugname")
    public Msg findDrugImgByMachineDrugname(@RequestParam(value = "dname",defaultValue = "")String dname,@RequestParam("mhid")String mhid){
        PageHelper.startPage(1,50);
        HashMap<String, String> map = new HashMap<>();
        map.put("drname",dname);
        map.put("mhid",mhid);
        List<Drug> drugs = drugService.findDrugImgByMachineDrugname(map);
        PageInfo page = new PageInfo(drugs,5);
        return Msg.success().add("PageInfo",page);
    }
    //根据药品ids和机器号删除相应的药品
    @ResponseBody
    @RequestMapping(value = "/deleteDrugByIdsMachine",method = RequestMethod.POST)
    public Msg deleteDrugByIdsMachine(String ids,String mhid){
        String[] split = ids.split("-");
        ArrayList idsList = new ArrayList();
        for (String s : split) {
            idsList.add(s);
        }
        HashMap map = new HashMap();
        map.put("ids",idsList);
        map.put("mhid",mhid);
        drugService.deleteDrugByIdsMachine(map);
        return Msg.success();
    }
    //检验药品编号是否已经存在,若存在不可添加
    @ResponseBody
    @RequestMapping("/checkDrugDrid")
    public Msg checkDrug(@RequestParam("drid") String drid){
        ArrayList drugId = new ArrayList();
        drugId.add(Integer.valueOf(drid));
        List<Drug> drugs = drugService.findDrugByIds(drugId);
        System.out.println(drugs.size());
        if(drugs.size()>0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
    }
    @ResponseBody
    @RequestMapping("/checkMachineDrug")
    public Msg checkMachineDrug(String drname,String mhid){
        HashMap<String, String> map = new HashMap<>();
        map.put("drname",drname);
        map.put("mhid",mhid);
        //如果药品名未在规格中存在，或药品在此机器中已经存在，则返回失败代码
        if((specificationService.findSpecificationsByNamesAccurate(drname)==null)||(drugService.findDrugImgByMachineDrugnameAccurate(map)!=null)){
            return Msg.fali();
        }else {
            return Msg.success();
        }
    }
    @ResponseBody
    @RequestMapping("/deleteDrugByIds")
    public Msg deleteDrugByIds(@RequestParam(value ="drids" )String drids){
       String[] split = drids.split("-");
       ArrayList<Integer> list=new ArrayList<>();
       for (String s : split) {
           list.add(Integer.parseInt(s));
       }
       int i = drugService.deleteDrugByIds(list);
        if(i==0){
            return Msg.fali();
        }else{
            return Msg.success();
        }
   }
    //新增药品
    @ResponseBody
    @RequestMapping(value = "/machineDrugAdd")
    public Msg machineDrugAdd(String drid, String drname, Double drprice, Integer drnumber, String drproduction, String drexpiration,String mhid) throws ParseException {
        ToolsMapper toolsMapper = new ToolsMapper();
        Date drproductionDate = toolsMapper.stringToDateSimple(drproduction);
        Date drexpirationDate = toolsMapper.stringToDateSimple(drexpiration);
        Drug drug = new Drug(drid, drname, drprice, drnumber, drproductionDate, drexpirationDate, null, mhid);
        int i = drugService.addDrug(drug);
        if (i != 0) {
            return Msg.success();
        } else
            return Msg.fali();
        }
}

