package com.t.controller;

import com.t.pojo.Drug;
import com.t.pojo.Msg;
import com.t.service.drugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/wxDrug")
public class wechatDrugController {
    @Autowired
    @Qualifier("drugServiceImpl")
    private com.t.service.drugService drugService;

    @ResponseBody
    @RequestMapping("/findDrugByMachineId")
    public Msg findDrugByMachineId(Integer mhid){
        List<Drug> drug = drugService.findDrugByMachineId1(mhid);
        return Msg.success().add("drug",drug);
    }
}
