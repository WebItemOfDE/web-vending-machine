package com.t.service;

import com.t.pojo.Drug;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface drugService {
    List<Drug> findDrugAll();
    int addDrugs(List<Drug> list);
    int addDrug(Drug drug);
    int updateDrug(Drug drug);
    int deleteDrugById(String drid);
    int deleteDrugByIds(List<Integer> list);
    List<Drug> findDrugByCondition(HashMap<String,String> map);
    List<Drug> findDrugByIds(ArrayList list);
    List<Drug> findDrugByMachineId(ArrayList list);
    List<Drug> findDrugByMachineId1(int mhid);
    List<Drug> findDrugByMachineId2(String mhid);
    List<Drug> findDrugImgByMachineId(String mhid);
    List<Drug> findDrugImgByMachineDrugname(HashMap<String,String> map);
    int deleteDrugByIdsMachine(HashMap map);
    //业务描述，后端传入一个药品名称，和机器号，先查询药品在规格中是否存在，再查询药品在机器中是否已经存在
    Drug findDrugImgByMachineDrugnameAccurate(HashMap<String,String> map);
}
