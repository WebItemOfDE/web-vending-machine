package com.t.service;

import com.t.pojo.ChartPresentation;

import java.util.List;

public interface chartPresentationService {

    List<ChartPresentation> findChartPresentationByGroup();
}
