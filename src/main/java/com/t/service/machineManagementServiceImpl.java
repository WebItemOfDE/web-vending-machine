package com.t.service;

import com.t.dao.machineManagementMapper;
import com.t.pojo.Drug;
import com.t.pojo.Machine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service()
public class machineManagementServiceImpl implements machineManagementService {
    @Autowired
    private machineManagementMapper mapper;


    public machineManagementMapper getMapper() {
        return mapper;
    }

    public void setMapper(machineManagementMapper mapper) {
        this.mapper = mapper;
    }
    public void setMachineManagementMapper(machineManagementMapper machineManagementMapper) {
        this.mapper=machineManagementMapper;
    }

    @Override
    public int addMachine(Machine machine) {
        return mapper.addMachine(machine);
    }

    @Override
    public int updateMachine(Machine machine) {
        return mapper.updateMachine(machine);
    }

    @Override
    public int deleteMachine(Machine machine) {
        return mapper.deleteMachine(machine);
    }

    @Override
    public int deleteMachines(List<Machine> machines) {
        return mapper.deleteMachines(machines);
    }

    @Override
    public List<Machine> findAll() {
        return mapper.findAll();
    }

    @Override
    public List<Machine> findByName(String mhname) {
        return mapper.findByName(mhname);
    }

    @Override
    public List<Machine> findMachineByIds(ArrayList list){
        return mapper.findMachineByIds(list);
    }

    @Override
    public int deleteMachineById(String mhid) {
        return mapper.deleteMachineById(mhid);
    }

    @Override
    public int deleteMachineByIds(List<Integer> list) {
        return mapper.deleteMachineByIds(list);
    }

    @Override
    public List<Machine> findMachineByCondition(HashMap<String, String> map) {
        return mapper.findMachineByCondition(map);
    }
}
