package com.t.service;

import com.t.pojo.Drug;
import com.t.pojo.Machine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface machineManagementService {
    //添加机器
    int addMachine(Machine machine);
    //修改
    int updateMachine(Machine machine);
    //删除单个
    int deleteMachine(Machine machine);
    //删除多个
    int deleteMachines(List<Machine> machines);
    //查找所有
    List<Machine> findAll();
    //查找单个
    List<Machine> findByName(String mhname);

    List<Machine> findMachineByIds(ArrayList list);

    int deleteMachineById(String mhid);
    int deleteMachineByIds(List<Integer> list);

    List<Machine> findMachineByCondition(HashMap<String,String> map);


}
