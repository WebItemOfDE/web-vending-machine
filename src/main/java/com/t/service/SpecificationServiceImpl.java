package com.t.service;

import com.t.dao.SpecificationMapper;
import com.t.pojo.Specifications;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SpecificationServiceImpl implements SpecificationService{
    @Autowired
    private SpecificationMapper specificationMapper;

    public void setSpecificationMapper(SpecificationMapper specificationMapper) {
        this.specificationMapper = specificationMapper;
    }

    @Override
    public List<Specifications> findSpecificationsAll() {
        return specificationMapper.findSpecificationsAll();
    }

    @Override
    public List<Specifications> findSpecificationsByNames(String name) {
        return specificationMapper.findSpecificationsByNames(name);
    }

    @Override
    public int deleteSpecificationsById(String name) {
        return specificationMapper.deleteSpecificationsById(name);
    }

    @Override
    public int addSpecifications(Specifications specifications) {
        return specificationMapper.addSpecifications(specifications);
    }

    @Override
    public Specifications findSpecificationsByNamesAccurate(String name) {
        return specificationMapper.findSpecificationsByNamesAccurate(name);
    }

    @Override
    public int updateSpecificationsByname(Specifications specifications) {
        return specificationMapper.updateSpecificationsByname(specifications);
    }

    @Override
    public int deleteSpecificationsByName(String name) {
        return specificationMapper.deleteSpecificationsByName(name);
    }

    @Override
    public int deleteSpecificationsByNames(List<String> names) {
        return specificationMapper.deleteSpecificationsByNames(names);
    }
}
