package com.t.service;

import com.t.pojo.User;

import java.util.List;

public interface userService {
    User SelectUserByAccount(String account) ;
    User loginCheck(User user);
    List<User> findUserAll();
}
