package com.t.service;

import com.t.dao.chartMapper;
import com.t.pojo.Drug;
import com.t.pojo.SaleChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class chartServiceImpl implements chartService{
    private chartMapper chartMapper;

    public void setChartMapper(chartMapper chartMapper) {
        this.chartMapper = chartMapper;
    }


    @Override
    public List<SaleChart> findChartByGroup() {
        return chartMapper.findChartByGroup();
    }
}
