package com.t.service;

import com.t.dao.saleMapper;
import com.t.pojo.Sale;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class saleServiceImpl implements saleService{
    @Autowired
    saleMapper saleMapper;

    public void setSaleMapper(com.t.dao.saleMapper saleMapper) {
        this.saleMapper = saleMapper;
    }

    @Override
    public int addSale(Sale sale) {
        return saleMapper.addSale(sale);
    }

    @Override
    public List<Sale> findSaleAll() {
        return saleMapper.findSaleAll();
    }

    @Override
    public List<Sale> findSaleByNameDate(HashMap map) {
        return saleMapper.findSaleByNameDate(map);
    }

    @Override
    public int deleteSaleByNums(ArrayList<String> nums) {
        return saleMapper.deleteSaleByNums(nums);
    }
}
