package com.t.service;

import com.t.dao.userMapper;
import com.t.pojo.User;

import java.util.List;

public class userServiceImpl implements userService{
    private userMapper userMapper;

    public void setUserMapper(com.t.dao.userMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User SelectUserByAccount(String account) {
        return userMapper.SelectUserByAccount(account);
    }

    @Override
    public User loginCheck(User user) {
        return userMapper.loginCheck(user);
    }

    @Override
    public List<User> findUserAll() {
        return userMapper.findUserAll();
    }


}
