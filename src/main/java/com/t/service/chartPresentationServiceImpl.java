package com.t.service;

import com.t.dao.chartPresentationMapper;
import com.t.pojo.ChartPresentation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class chartPresentationServiceImpl implements chartPresentationService {

    private chartPresentationMapper chartPresentationMapper;

    public void setChartPresentationMapper(chartPresentationMapper chartPresentationMapper) {
        this.chartPresentationMapper = chartPresentationMapper;
    }

    @Override
    public List<ChartPresentation> findChartPresentationByGroup() {
        return chartPresentationMapper.findChartPresentationByGroup();
    }
}
