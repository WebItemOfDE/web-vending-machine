package com.t.service;

import com.t.pojo.Drug;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class drugServiceImpl  implements  drugService{
    private com.t.dao.drugMapper drugMapper;

    public void setDrugMapper(com.t.dao.drugMapper drugMapper) {
        this.drugMapper = drugMapper;
    }


    @Override
    public List<Drug> findDrugAll() {
        return drugMapper.findDrugAll();
    }

    @Override
    public int addDrugs(List<Drug> list) {
        return drugMapper.addDrugs(list);
    }

    @Override
    public int addDrug(Drug drug) {
        return drugMapper.addDrug(drug);
    }

    @Override
    public int updateDrug(Drug drug) {
        return drugMapper.updateDrug(drug);
    }

    @Override
    public int deleteDrugById(String drid) {
        return drugMapper.deleteDrugById(drid);
    }

    @Override
    public int deleteDrugByIds(List<Integer> list) {
        return drugMapper.deleteDrugByIds(list);
    }

    @Override
    public List<Drug> findDrugByCondition(HashMap<String, String> map) {
        return drugMapper.findDrugByCondition(map);
    }

    @Override
    public List<Drug> findDrugByIds(ArrayList list) {
        return drugMapper.findDrugByIds(list);
    }

    @Override
    public List<Drug> findDrugByMachineId(ArrayList list) {
        return drugMapper.findDrugByMachineId(list);
    }

    @Override
    public List<Drug> findDrugByMachineId1(int mhid) {
        return drugMapper.findDrugByMachineId1(mhid);
    }

    @Override
    public List<Drug> findDrugByMachineId2(String mhid) {
        return drugMapper.findDrugByMachineId2(mhid);
    }

    @Override
    public List<Drug> findDrugImgByMachineId(String mhid) {
        return drugMapper.findDrugImgByMachineId(mhid);
    }

    @Override
    public List<Drug> findDrugImgByMachineDrugname(HashMap<String, String> map) {
        return drugMapper.findDrugImgByMachineDrugname(map);
    }

    @Override
    public int deleteDrugByIdsMachine(HashMap map) {
        return drugMapper.deleteDrugByIdsMachine(map);
    }

    @Override
    public Drug findDrugImgByMachineDrugnameAccurate(HashMap<String, String> map) {
        return drugMapper.findDrugImgByMachineDrugnameAccurate(map);
    }


}
