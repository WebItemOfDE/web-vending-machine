package com.t.service;

import com.t.pojo.Specifications;
import org.springframework.stereotype.Service;

import java.util.List;

public interface SpecificationService {
    List<Specifications> findSpecificationsAll();
    List<Specifications> findSpecificationsByNames(String name);
    int deleteSpecificationsById(String name);
    int addSpecifications(Specifications specifications);
    Specifications findSpecificationsByNamesAccurate(String name);
    int updateSpecificationsByname(Specifications specifications);
    int deleteSpecificationsByName(String name);
    int deleteSpecificationsByNames(List<String> names);
}
