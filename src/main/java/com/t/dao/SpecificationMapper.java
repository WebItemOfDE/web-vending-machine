package com.t.dao;

import com.t.pojo.Specifications;

import java.util.List;

public interface SpecificationMapper {
    List<Specifications> findSpecificationsAll();
    List<Specifications> findSpecificationsByNames(String name);
    int deleteSpecificationsById(String name);
    int addSpecifications(Specifications specifications);
    //准确通过姓名查找信息
    Specifications findSpecificationsByNamesAccurate(String name);
    int updateSpecificationsByname(Specifications specifications);
    int deleteSpecificationsByName(String name);
    int deleteSpecificationsByNames(List<String> names);
}
