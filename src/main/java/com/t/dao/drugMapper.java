package com.t.dao;

import com.t.pojo.Drug;
import com.t.pojo.User;

import java.util.*;

public interface drugMapper {
        //药品管理界面
        List<Drug> findDrugAll();
        int addDrugs(List<Drug> list);
        int addDrug(Drug drug);
        int updateDrug(Drug drug);
        int deleteDrugById(String drid);
        int deleteDrugByIds(List<Integer> list);
        List<Drug> findDrugByCondition(HashMap<String,String> map);
        List<Drug> findDrugByIds(ArrayList list);
        List<Drug> findDrugByMachineId(ArrayList list);
        List<Drug> findDrugByMachineId1(int mhid);
        //微信小程序
        List<Drug> findDrugByMachineId2(String mhid);
        //机器药品界面
        List<Drug> findDrugImgByMachineId(String mhid);
        List<Drug> findDrugImgByMachineDrugname(HashMap<String,String> map);
        //根据确定的药品名和机器号查询相应的药品
        Drug findDrugImgByMachineDrugnameAccurate(HashMap<String,String> map);
        int deleteDrugByIdsMachine(HashMap map);

}
