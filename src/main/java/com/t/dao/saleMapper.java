package com.t.dao;

import com.t.pojo.Sale;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface saleMapper {
     int addSale(Sale sale);
     List<Sale> findSaleAll();
     List<Sale> findSaleByNameDate(HashMap map);
     int deleteSaleByNums(ArrayList<String> nums);
}
