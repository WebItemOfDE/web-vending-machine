package com.t.dao;

import com.t.pojo.Drug;
import com.t.pojo.SaleChart;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface chartMapper {
    List<SaleChart> findChartByGroup();
}
