package com.t.dao;

import com.t.pojo.ChartPresentation;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface chartPresentationMapper {

    List<ChartPresentation> findChartPresentationByGroup();

}
