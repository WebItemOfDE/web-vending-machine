package com.t.pojo;

public class ChartPresentation {

    private String mhname;
    private double sale_count;

    public ChartPresentation(String mhname, double sale_count) {
        this.mhname = mhname;
        this.sale_count = sale_count;
    }

    public ChartPresentation() {
    }

    public String getMhname() {
        return mhname;
    }

    public void setMhname(String mhname) {
        this.mhname = mhname;
    }

    public double getSale_count() {
        return sale_count;
    }

    public void setSale_count(double sale_count) {
        this.sale_count = sale_count;
    }
}
