package com.t.pojo;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Drug {
    private String drid;
    private String drname;
    private double drprice;
    private int drnumber;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date drproduction;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date drexpiration;
    private String imgurl;
    private String  mhid;
    private Specifications specification;

    public Specifications getSpecification() {
        return specification;
    }

    public void setSpecification(Specifications specification) {
        this.specification = specification;
    }

    public void setDrprice(double drprice) {
        this.drprice = drprice;
    }

    public String getMhid() {
        return mhid;
    }

    public void setMhid(String mhid) {
        this.mhid = mhid;
    }

    public String getDrid() {
        return drid;
    }

    public void setDrid(String drid) {
        this.drid = drid;
    }

    public String getDrname() {
        return drname;
    }

    public void setDrname(String drname) {
        this.drname = drname;
    }

    public double getDrprice() {
        return drprice;
    }

    public void setDrprice(float drprice) {
        this.drprice = drprice;
    }

    public int getDrnumber() {
        return drnumber;
    }

    public void setDrnumber(int drnumber) {
        this.drnumber = drnumber;
    }

    public Date getDrproduction() {
        return drproduction;
    }

    public void setDrproduction(Date drproduction) {
        this.drproduction = drproduction;
    }

    public Date getDrexpiration() {
        return drexpiration;
    }

    public void setDrexpiration(Date drexpiration) {
        this.drexpiration = drexpiration;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Drug(String drid, String drname, double drprice, int drnumber, Date drproduction, Date drexpiration, String imgurl, String mhid, Specifications specification) {
        this.drid = drid;
        this.drname = drname;
        this.drprice = drprice;
        this.drnumber = drnumber;
        this.drproduction = drproduction;
        this.drexpiration = drexpiration;
        this.imgurl = imgurl;
        this.mhid = mhid;
        this.specification = specification;
    }
    public Drug(String drid, String drname, double drprice, int drnumber, Date drproduction, Date drexpiration, String imgurl, String mhid) {
        this.drid = drid;
        this.drname = drname;
        this.drprice = drprice;
        this.drnumber = drnumber;
        this.drproduction = drproduction;
        this.drexpiration = drexpiration;
        this.imgurl = imgurl;
        this.mhid = mhid;
    }

    public Drug() {

    }

    @Override
    public String toString() {
        return "Drug{" +
                "drid='" + drid + '\'' +
                ", drname='" + drname + '\'' +
                ", drprice=" + drprice +
                ", drnumber=" + drnumber +
                ", drproduction=" + drproduction +
                ", drexpiration=" + drexpiration +
                ", imgurl='" + imgurl + '\'' +
                ", mhid='" + mhid + '\'' +
                '}';
    }

}
