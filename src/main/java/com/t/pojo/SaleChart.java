package com.t.pojo;

public class SaleChart {
    private String dname;
    private double sale_count;

    public SaleChart() {
    }

    public SaleChart(String dname, double sale_count) {
        this.dname = dname;
        this.sale_count = sale_count;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public double getSale_count() {
        return sale_count;
    }

    public void setSale_count(double sale_count) {
        this.sale_count = sale_count;
    }
}
