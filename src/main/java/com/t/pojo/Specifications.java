package com.t.pojo;

public class Specifications {
    private String drname;
    private String drspecifications;
    private String drfunction;
    private String drintroduction;
    private String dimgurl;

    public Specifications() {
    }

    public Specifications(String drname, String drspecifications, String drfunction, String drintroduction, String dimgurl) {
        this.drname = drname;
        this.drspecifications = drspecifications;
        this.drfunction = drfunction;
        this.drintroduction = drintroduction;
        this.dimgurl = dimgurl;
    }
    public Specifications(String drname, String drspecifications, String drfunction, String drintroduction) {
        this.drname = drname;
        this.drspecifications = drspecifications;
        this.drfunction = drfunction;
        this.drintroduction = drintroduction;
    }

    public String getDrname() {
        return drname;
    }

    public void setDrname(String drname) {
        this.drname = drname;
    }

    public String getDrspecifications() {
        return drspecifications;
    }

    public void setDrspecifications(String drspecifications) {
        this.drspecifications = drspecifications;
    }

    public String getDrfunction() {
        return drfunction;
    }

    public void setDrfunction(String drfunction) {
        this.drfunction = drfunction;
    }

    public String getDrintroduction() {
        return drintroduction;
    }

    public void setDrintroduction(String drintroduction) {
        this.drintroduction = drintroduction;
    }

    public String getDimgurl() {
        return dimgurl;
    }

    public void setDimgurl(String dimgurl) {
        this.dimgurl = dimgurl;
    }

    @Override
    public String toString() {
        return "Specifications{" +
                "drname='" + drname + '\'' +
                ", drspecifications='" + drspecifications + '\'' +
                ", drfunction='" + drfunction + '\'' +
                ", drintroduction='" + drintroduction + '\'' +
                ", dimgurl='" + dimgurl + '\'' +
                '}';
    }
}
