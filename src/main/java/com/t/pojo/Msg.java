package com.t.pojo;

import java.util.HashMap;
import java.util.Map;

//通用的返回类，返回json数据或者是否成功删除插入
public class Msg {
    //状态码 100成功  200失败
    private int code;
    //提示信息
    private String msg;
    //用户返回给浏览器的数据
    private Map<String,Object>  extend=new HashMap<String, Object>();
    //处理成功时返回的数据
    public static Msg success(){
        Msg result = new Msg();
        result.setCode(100);
        result.setMsg("处理成功");
        return result;
    }
    //处理失败时返回的数据
    public static Msg fali(){
        Msg result = new Msg();
        result.setCode(200);
        result.setMsg("处理失败");
        return result;
    }
//    返回的JSON数据全部用msg代替,所以要让msg中的map能添加字符串
    public Msg add(String key,Object value){
         this.extend.put(key, value);
         return this;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Map<String, Object> getExtend() {
        return extend;
    }

    public void setExtend(Map<String, Object> extend) {
        this.extend = extend;
    }
}
