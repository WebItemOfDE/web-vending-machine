package com.t.pojo;

import java.util.Date;

public class Sale {
    private String orderNumber;
    private String dname;
    private String mhid;
    private double salePrice;
    private int saleNumber;
    private Date saleDate;
    private String saleUseropenid;
    public Sale(String orderNumber, String dname, String mhid, double salePrice, int saleNumber, Date saleDate, String saleUseropenid) {
        this.orderNumber = orderNumber;
        this.dname = dname;
        this.mhid = mhid;
        this.salePrice = salePrice;
        this.saleNumber = saleNumber;
        this.saleDate = saleDate;
        this.saleUseropenid = saleUseropenid;
    }

    public Sale() {

    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDname() {
        return dname;
    }
    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getMhid() {
        return mhid;
    }

    public void setMhid(String mhid) {
        this.mhid = mhid;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public int getSaleNumber() {
        return saleNumber;
    }

    public void setSaleNumber(int saleNumber) {
        this.saleNumber = saleNumber;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getSaleUseropenid() {
        return saleUseropenid;
    }

    public void setSaleUseropenid(String saleUseropenid) {
        this.saleUseropenid = saleUseropenid;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "orderNumber='" + orderNumber + '\'' +
                ", dname='" + dname + '\'' +
                ", mhid='" + mhid + '\'' +
                ", salePrice=" + salePrice +
                ", saleNumber=" + saleNumber +
                ", saleDate=" + saleDate +
                ", saleUseropenid='" + saleUseropenid + '\'' +
                '}';
    }
}
