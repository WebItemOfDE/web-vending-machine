package com.t.pojo;

import lombok.ToString;

import java.util.ArrayList;

@ToString
public class Machine {
  private String mhid;
  private String mhname;
  private String mhipadrress;

  public Machine() {
  }

  public Machine(String mhid, String mhname, String mhipadrress, String drugdata) {
    this.mhid = mhid;
    this.mhname = mhname;
    this.mhipadrress = mhipadrress;
  }

  public String getMhid() {
    return mhid;
  }

  public void setMhid(String mhid) {
    this.mhid = mhid;
  }

  public String getMhname() {
    return mhname;
  }

  public void setMhname(String mhname) {
    this.mhname = mhname;
  }

  public String getMhipadrress() {
    return mhipadrress;
  }

  public void setMhipadrress(String mhipadrress) {
    this.mhipadrress = mhipadrress;
  }

  @Override
  public String toString() {
    return "Machine{" +
            "mhid='" + mhid + '\'' +
            ", mhname='" + mhname + '\'' +
            ", mhipadrress='" + mhipadrress + '\'' +
            '}';
  }
}
