<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>图表展示</title>
    <link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
    <meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
    <meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
    <meta name="author" content="yinqi">
    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>

<body>
<div class="lyear-layout-web">
    <div class="lyear-layout-container">
        <!--左侧导航-->
        <!--End 左侧导航-->
        <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>

        <!--头部信息-->
        <header class="lyear-layout-header">

            <nav class="navbar navbar-default">
                <div class="topbar">

                    <div class="topbar-left">
                        <div class="lyear-aside-toggler">
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                        </div>
                        <span class="navbar-page-title"> 后台首页 </span>
                    </div>


                    <ul class="topbar-right">
                        <li class="dropdown dropdown-profile">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <span>我的 <span class="caret"></span></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li> <a href="lyear_pages_profile.html"><i class="mdi mdi-account"></i> 个人信息</a> </li>
                                <li> <a href="lyear_pages_edit_pwd.html"><i class="mdi mdi-lock-outline"></i> 修改密码</a> </li>
                                <li> <a href="javascript:void(0)"><i class="mdi mdi-delete"></i> 清空缓存</a></li>
                                <li class="divider"></li>
                                <li> <a href="lyear_pages_login.html"><i class="mdi mdi-logout-variant"></i> 退出登录</a> </li>
                            </ul>
                        </li>
                        <!--切换主题配色-->
                        <li class="dropdown dropdown-skin">
                            <span data-toggle="dropdown" class="icon-palette"><i class="mdi mdi-palette"></i></span>
                            <ul class="dropdown-menu dropdown-menu-right" data-stopPropagation="true">
                                <li class="drop-title"><p>主题</p></li>
                                <li class="drop-skin-li clearfix">
                  <span class="inverse">
                    <input type="radio" name="site_theme" value="default" id="site_theme_1" checked>
                    <label for="site_theme_1"></label>
                  </span>
                                    <span>
                    <input type="radio" name="site_theme" value="dark" id="site_theme_2">
                    <label for="site_theme_2"></label>
                  </span>
                                    <span>
                    <input type="radio" name="site_theme" value="translucent" id="site_theme_3">
                    <label for="site_theme_3"></label>
                  </span>
                                </li>
                                <li class="drop-title"><p>头部</p></li>
                                <li class="drop-skin-li clearfix">
                  <span class="inverse">
                    <input type="radio" name="header_bg" value="default" id="header_bg_1" checked>
                    <label for="header_bg_1"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_2" id="header_bg_2">
                    <label for="header_bg_2"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_3" id="header_bg_3">
                    <label for="header_bg_3"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_4" id="header_bg_4">
                    <label for="header_bg_4"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_5" id="header_bg_5">
                    <label for="header_bg_5"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_6" id="header_bg_6">
                    <label for="header_bg_6"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_7" id="header_bg_7">
                    <label for="header_bg_7"></label>
                  </span>
                                    <span>
                    <input type="radio" name="header_bg" value="color_8" id="header_bg_8">
                    <label for="header_bg_8"></label>
                  </span>
                                </li>
                                <li class="drop-title"><p>侧边栏</p></li>
                                <li class="drop-skin-li clearfix">
                  <span class="inverse">
                    <input type="radio" name="sidebar_bg" value="default" id="sidebar_bg_1" checked>
                    <label for="sidebar_bg_1"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_2" id="sidebar_bg_2">
                    <label for="sidebar_bg_2"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_3" id="sidebar_bg_3">
                    <label for="sidebar_bg_3"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_4" id="sidebar_bg_4">
                    <label for="sidebar_bg_4"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_5" id="sidebar_bg_5">
                    <label for="sidebar_bg_5"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_6" id="sidebar_bg_6">
                    <label for="sidebar_bg_6"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_7" id="sidebar_bg_7">
                    <label for="sidebar_bg_7"></label>
                  </span>
                                    <span>
                    <input type="radio" name="sidebar_bg" value="color_8" id="sidebar_bg_8">
                    <label for="sidebar_bg_8"></label>
                  </span>
                                </li>
                            </ul>
                        </li>
                        <!--切换主题配色-->
                    </ul>

                </div>
            </nav>

        </header>
        <!--End 头部信息-->

        <!--页面主要内容-->
        <main class="lyear-layout-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>销售额</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-bars"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>销售趋势</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-lines"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>商品销售占比</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-bars1"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4>TOP销售机器</h4>
                            </div>
                            <div class="card-body">
                                <canvas class="js-chartjs-lines1"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!--End 页面主要内容-->
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/Chart.js"></script>
<script type="text/javascript">
    'use strict'
     let labels1 = [], data1=[];
     $(document).ready(function (e) {
         $.ajax({
            url:"${pageContext.request.contextPath}/presentation/ChartPresentation",
            type:"Get",
            success:function (result) {
                console.log(result);
                var charts = result.extend.ChartPresentation;
                // 将获取到的json数据分别存放到两个数组中
                for(let chartrecord in charts){
                    labels1.push(charts[chartrecord].mhname);
                    data1.push(charts[chartrecord].sale_count);
                }




                var $dashChartBarsCnt  = jQuery( '.js-chartjs-bars' )[0].getContext( '2d' ),
                $dashChartLinesCnt = jQuery( '.js-chartjs-lines' )[0].getContext( '2d' );
                var $dashChartBarsData = {
                    labels: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                    datasets: [
                        {
                            label: '销售额',
                            borderWidth: 1,
                            borderColor: 'rgba(0,0,0,0)',
                            backgroundColor: 'rgba(51,202,185,0.5)',
                            hoverBackgroundColor: "rgba(51,202,185,0.7)",
                            hoverBorderColor: "rgba(0,0,0,0)",
                            data: [2500, 1500, 1200, 3200, 4800, 3500, 1500]
                        }
                    ]
                };
                var $dashChartLinesData = {
                    labels: ['2021-4', '2021-3', '2021-2', '2021-1', '2020-12', '2020-11', '2020-10', '2020-9', '2020-8', '2020-7', '2020-6', '2020-5'],
                    datasets: [
                        {
                            label: '销售趋势',
                            data: [20, 25, 40, 30, 45, 40, 55, 40, 48, 40, 42, 50],
                            borderColor: '#358ed7',
                            backgroundColor: 'rgba(53, 142, 215, 0.175)',
                            borderWidth: 1,
                            fill: false,
                            lineTension: 0.5
                        }
                    ]
                };
                new Chart($dashChartBarsCnt, {
                    type: 'line',
                    data: $dashChartBarsData
                });
              new Chart($dashChartLinesCnt, {
                        type: 'line',
                        data: $dashChartLinesData,
               });
                var $dashChartBarsCnt1  = jQuery( '.js-chartjs-bars1' )[0].getContext( '2d' ),
                    $dashChartLinesCnt1 = jQuery( '.js-chartjs-lines1' )[0].getContext( '2d' );
                var $dashChartBarsData1 = {
                    labels: [
                        '999感冒灵',
                        '丹参酮胶囊',
                        '同康他所乳膏'
                    ],
                    datasets: [{
                        label: 'My First Dataset',
                        data: [300, 50, 100],
                        backgroundColor: [
                            'rgb(255, 99, 132)',
                            'rgb(54, 162, 235)',
                            'rgb(255, 205, 86)'
                        ],
                        hoverOffset: 4
                    }]
                };
                var $dashChartLinesData1 = {
                    labels: labels1,
                    datasets: [
                        {
                            label: 'TOP销售机器',
                            data: data1,
                            borderColor: '#358ed7',
                            backgroundColor: 'rgba(53, 142, 215, 0.175)',
                            borderWidth: 1,
                            fill: false,
                            lineTension: 0.5
                        }
                    ]
                };
                new Chart($dashChartBarsCnt1, {
                    type: 'doughnut',
                    data: $dashChartBarsData1
                });
                new Chart($dashChartLinesCnt1, {
                    type: 'bar',
                    data: $dashChartLinesData1,
                });


           }
        })
    })

























    $(function(){
        $('.search-bar .dropdown-menu a').click(function() {
            var field = $(this).data('field') || '';
            $('#search-field').val(field);
            $('#search-btn').html($(this).text() + ' <span class="caret"></span>');
        });
    });
    $(document).ready(function(e) {
        // 使用时，请自行引入jquery.cookie.js
        // 读取cookie中的主题设置
        var the_logo_bg    = $.cookie('the_logo_bg'),
            the_header_bg  = $.cookie('the_header_bg'),
            the_sidebar_bg = $.cookie('the_sidebar_bg'), // iframe版本如果删除了下面一行，请把这一行的逗号改成分号
            the_site_theme = $.cookie('the_site_theme'); // iframe版本可不需要这行

        if (the_logo_bg) $('body').attr('data-logobg', the_logo_bg);
        if (the_header_bg) $('body').attr('data-headerbg', the_header_bg);
        if (the_sidebar_bg) $('body').attr('data-sidebarbg', the_sidebar_bg);
        if (the_site_theme) $('body').attr('data-theme', the_site_theme); // iframe版本可不需要这行

        // 处理主题配色下拉选中
        $(".dropdown-skin :radio").each(function(){
            var $this = $(this),
                radioName = $this.attr('name');
            switch (radioName) {
                case 'site_theme':
                    $this.val() == the_site_theme && $this.prop("checked", true);
                    break;  // iframe版中不需要这个case
                case 'logo_bg':
                    $this.val() == the_logo_bg && $this.prop("checked", true);
                    break;
                case 'header_bg':
                    $this.val() == the_header_bg && $this.prop("checked", true);
                    break;
                case 'sidebar_bg':
                    $this.val() == the_sidebar_bg && $this.prop("checked", true);
            }
        });

        // 设置主题配色
        setTheme = function(input_name, data_name) {
            $("input[name='"+input_name+"']").click(function(){
                $('body').attr(data_name, $(this).val());
                $.cookie('the_'+input_name, $(this).val());
            });
        }
        setTheme('site_theme', 'data-theme'); // iframe版本可不需要这行
        setTheme('logo_bg', 'data-logobg');
        setTheme('header_bg', 'data-headerbg');
        setTheme('sidebar_bg', 'data-sidebarbg');
    });
<%--    $(document).ready(function (e) {--%>
<%--        --%>
<%--    })--%>
</script>
</body>
</html>