<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>所有商品</title>
    <link rel="icon" href="${pageContext.request.contextPath}/pages/favicon.ico" type="image/ico">
    <meta name="keywords" content="LightYear,光年,后台模板,后台管理系统,光年HTML模板">
    <meta name="description" content="LightYear是一个基于Bootstrap v3.3.7的后台管理系统的HTML模板。">
    <meta name="author" content="yinqi">
    <link href="${pageContext.request.contextPath}/pages/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/pages/css/style.min.css" rel="stylesheet">
</head>
<body>
<div class="lyear-layout-web">
    <div class="lyear-layout-container">
        <!--左侧导航-->
      <%@ include file="/pages/static/jsp/leftNavigation.jsp" %>
        <!--End 左侧导航-->

        <!--头部信息-->
        <header class="lyear-layout-header">

            <nav class="navbar navbar-default">
                <div class="topbar">

                    <div class="topbar-left">
                        <div class="lyear-aside-toggler">
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                            <span class="lyear-toggler-bar"></span>
                        </div>
                        <span class="navbar-page-title"> 后台首页 </span>
                    </div>
                  <%@include file="/pages/static/jsp/rightNavigation.jsp"%>
                </div>
            </nav>

        </header>
        <!--End 头部信息-->
        <main class="lyear-layout-content">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-toolbar clearfix">
                                <form class="pull-right search-bar" method="get" action="#!" role="form">
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <input type="hidden" name="search_field" id="search-field" >
                                            <button class="btn btn-warning dropdown-toggle" id="search-btn" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-search-web"></i>  查找
                                            </button>
                                        </div>
                                        <input  type="text" class="form-control" name="keyword" placeholder="请输入商品名称" id="search_field1" style="solid-color: yellow">
                                    </div>
                                </form>
                                <div class="toolbar-btn-action">
                                    <btn class="btn btn-primary m-r-5"
                                         id="AllProducts_add"
                                         data-toggle="modal">
                                        <i class="mdi mdi-plus"></i> 新增</btn>
                                   
                                   
                                    <!--新增模态框-->
                                    <div class="modal fade" id="AllProducts_add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="pointer-events: auto">
                                        <div class="modal-dialog" style="overflow: auto;pointer-events: auto ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        添加商品
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <p>商品编号</p>
                                                        <input  type="text" class="form-control" value="" name="drid" placeholder="请输入商品编号" id="add_drid" style="solid-color: yellow">
                                                        <p style="color: red" id="add_resultP">该商品已经存在,请更换商品编号</p>
                                                        <p>商品名称</p>
                                                        <input  type="text" class="form-control" value="" name="drname" placeholder="请输入商品名称" id="add_drname" style="solid-color: yellow">
                                                        <p>商品价格</p>
                                                        <input  type="text" class="form-control" value="" name="drprice" placeholder="请输入商品价格" id="add_drprice" style="solid-color: yellow">
                                                        <p>商品库存</p>
                                                        <input  type="text" class="form-control" value="" name="drnumber" placeholder="请输入商品库存" id="add_drnumber" style="solid-color: yellow">
                                                        <p>商品生产日期</p>
                                                        <input  type="text" class="form-control" value="" name="drproduction" placeholder="请输入商品生产日期" id="add_drproductionDate" style="solid-color: yellow">
                                                        <span class="help-block"></span>
                                                        <p>商品过期日期</p>
                                                        <input  type="text" class="form-control" value="" name="drexpiration" placeholder="请输入商品过期日期" id="add_drexpirationDate" style="solid-color: yellow">
                                                        <span class="help-block"></span>
                                                        <p>商品所属机器</p>
                                                        <input  type="text" class="form-control" value="" name="mhid" placeholder="请输入商品所属机器" id="add_mhid" style="solid-color: yellow">
                                                        <span class="help-block"></span>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">关闭
                                                    </button>
                                                    <button type="button" class="btn btn-primary" id="product_modal_add_btn">
                                                        确认添加
                                                    </button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                  
                                  
                                    <!--修改按钮-->
                                    <btn class="btn btn-success m-r-5" id="AllProducts_change" data-toggle="modal" data-target="#AllProducts_change_modal"><i class="mdi mdi-grease-pencil"></i> 修改</btn>
                                    <!--                                未获取修改的项目的id-->
                                    <div class="modal fade" id="AllProducts_change_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-hidden="true">×
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel1">
                                                         商品信息修改
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form>
                                                        <p>商品编号</p>
                                                        <input  type="text" class="form-control" readonly="readonly" name="drid" placeholder="请输入商品名称" id="Drugdrid_update_input" style="solid-color: yellow">
                                                        <p>商品名称</p>
                                                        <input  type="text" class="form-control" value="" name="drname" placeholder="请输入商品名称" id="Drugdrname_update_input" style="solid-color: yellow">
                                                        <p>商品价格</p>
                                                        <input  type="text" class="form-control" value="" name="drprice" placeholder="请输入商品价格" id="Drugdrprice_update_input" style="solid-color: yellow">
                                                        <p>商品库存</p>
                                                        <input  type="text" class="form-control" value="" name="drnumber" placeholder="请输入商品库存" id="Drugdrnumber_update_input" style="solid-color: yellow">
                                                        <p>商品所属机器</p>
                                                        <input  type="text" class="form-control" value="" name="mhid" placeholder="请输入商品所属机器" id="Drugdrmhid_update_input" style="solid-color: yellow">

                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">关闭
                                                    </button>
                                                    <button type="button" class="btn btn-primary" id="AllProducts_change_modal_btn">
                                                        确认修改
                                                    </button>
                                                </div>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                    <btn class="btn btn-danger m-r-5" id="AllProducts_delete"><i class="mdi mdi-window-close"></i> 删除</btn>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>
                                                <label class="lyear-checkbox checkbox-primary">
                                                    <input type="checkbox" id="check-all"><span></span>
                                                </label>
                                            </th>
                                            <th>编号</th>
                                            <th>药品名称</th>
                                            <th>价格</th>
                                            <th>库存</th>
                                            <th>生产日期</th>
                                            <th>过期日期</th>
                                            <th>所属机器</th>
                                            <th>操作</th>
                                        </tr>
                                        </thead>
                                        <tbody id="products_tbody">

                                        </tbody>
                                    </table>
                                </div>
                              
                              
                                <ul class="pagination" id="products_pageInfo_area">
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </main>
    </div>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/main.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/pages/static/js/Initpolish.js"></script>
<script type="text/javascript">
    'use strict'
    var totalRecord,currentPage;
    $('#add_resultP').hide();
    $(function () {
        to_page(1);
    })
    function to_page(pn){
        $.ajax({
            url:"${pageContext.request.contextPath}/drug/AllProducts",
            data:"pn="+pn,
            type:"Get",
            success:function (result) {
                console.log(result);
                //遍历数据显示在表格中
                builld_drugs_table(result);
                builld_products_pageInfo_area(result);
            }
        })
    }
    //构建表格数据
    function builld_drugs_table(result){
           let drugs = result.extend.PageInfo.list;
           let html="";
           let checkBoxId="drugTableCheck";
           let date,date1;
           let deleteId="delete";
           let changeId="change";
           $.each(drugs,function (index,item) {
               checkBoxId=checkBoxId+item.drid;
               date= getdate(item.drproduction);
               date1=getdate(item.drexpiration);
               deleteId=deleteId+item.drid;
               changeId=changeId+item.drid;
                html=html+
                    "<tr>"
                    +"<td>"+"<label class='lyear-checkbox checkbox-primary'>"+"<input type='checkbox' name='ids' value='"+index+"' class='check_item' id='"+checkBoxId+"'>"+"<span>"+"</span>"+"</label>"+"</td>"
                    +"<td id='drid"+item.drid+"'>"+item.drid+"</td>"
                    +"<td>"+item.drname+"</td>"
                    +"<td>"+item.drprice+"</td>"
                    +"<td>"+item.drnumber+"</td>"
                    +"<td>"+date+"</td>"
                    +"<td>"+date1+"</td>"
                    +"<td>"+item.mhid+"</td>"
                    +"<td>"
                    +"<div class='btn btn-group'>"+
                             "<a class='btn btn-xs btn-default editProduct_btn' id='"+changeId+"' title='编辑' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-pencil'></i>"+"</a>"+
                             "<a class='btn btn-xs btn-default' title='查看' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-eye'></i>"+"</a>"+
                             "<a class='btn btn-xs btn-default deleteProduct_btn' id='"+deleteId+"' title='删除' data-toggle='tooltip' href='"+"#"+"'>"+"<i class='mdi mdi-window-close'></i>"+"</a>"+
                         "</div>"
                    +"</td>"+
                    +"</tr>"
               checkBoxId="drugTableCheck";
               deleteId="delete";
               changeId="change";
           })
        $('#products_tbody').html(html);
    }
    //构建分页区域
    function builld_products_pageInfo_area(result) {
            let html="";
            //判断还有没有上一页
            if(result.extend.PageInfo.hasPreviousPage==false){
                html=html+"<li class='disabled'  >"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
            }else {
                html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum-1)+")'>"+"<a href='#!'>"+"<<"+"</a>"+"</li>";
            }
            //遍历得到页码号
            $.each(result.extend.PageInfo.navigatepageNums,function (index,item) {
                //判断是不是当前页
                if(result.extend.PageInfo.pageNum==item){
                    html=html+"<li class='active' onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
                }else{
                    html=html+"<li onclick='to_page("+item+")'>"+"<a href='#!'>"+item+"</a>"+"</li>";
                }
            });
            //判断还有没有下一页
            if (result.extend.PageInfo.hasNextPage==false){
                html=html+"<li class='disabled' >"+"<a href='#!'>"+">>"+"</a>"+"</li>";
            }else {
                html=html+"<li onclick='to_page("+(result.extend.PageInfo.pageNum+1)+")'>"+"<a href='#!'>"+">>"+"</a>"+"</li>";
            }
            $('#products_pageInfo_area').html(html);
            totalRecord=result.extend.PageInfo.total;
            currentPage=result.extend.PageInfo.pageNum;
    }
    //按条件查询ajax
    window.onload=function () {
        $('#search-btn').click(function () {
            let file = document.getElementById("search_field1");
            let value = file.value;
            to_page_select(value,1);
        })
    }
    //切换到第几页
    function to_page_select(value,pn) {
        let data={pn:'',drname:''+value};
        $.ajax({
            url: "${pageContext.request.contextPath}/drug/selecProductsByCondition",
            data:data,
            dataType:'json',
            contentType : "application/json;charset=UTF-8",
            success:function (result) {
                builld_drugs_table(result);
            }
        })
    }
    //校验表单数据(校验数字未做,只做了日期)
    function check_Add_form_Data() {
        //校验日期
        let productionDate = document.getElementById("add_drproductionDate").value;
        let drexpirationDate = document.getElementById("add_drexpirationDate").value;
        let productionDate_check= productionDate.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))(-)(10|12|0?[13578])(-)(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))(-)(11|0?[469])(-)(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))(-)(0?2)(-)(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)(-)(0?2)(-)(29)$)|(^([3579][26]00)(-)(0?2)(-)(29)$)|(^([1][89][0][48])(-)(0?2)(-)(29)$)|(^([2-9][0-9][0][48])(-)(0?2)(-)(29)$)|(^([1][89][2468][048])(-)(0?2)(-)(29)$)|(^([2-9][0-9][2468][048])(-)(0?2)(-)(29)$)|(^([1][89][13579][26])(-)(0?2)(-)(29)$)|(^([2-9][0-9][13579][26])(-)(0?2)(-)(29)$))/);
        let drexpirationDate_check= drexpirationDate.match(/((^((1[8-9]\d{2})|([2-9]\d{3}))(-)(10|12|0?[13578])(-)(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))(-)(11|0?[469])(-)(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))(-)(0?2)(-)(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)(-)(0?2)(-)(29)$)|(^([3579][26]00)(-)(0?2)(-)(29)$)|(^([1][89][0][48])(-)(0?2)(-)(29)$)|(^([2-9][0-9][0][48])(-)(0?2)(-)(29)$)|(^([1][89][2468][048])(-)(0?2)(-)(29)$)|(^([2-9][0-9][2468][048])(-)(0?2)(-)(29)$)|(^([1][89][13579][26])(-)(0?2)(-)(29)$)|(^([2-9][0-9][13579][26])(-)(0?2)(-)(29)$))/);
        if(productionDate_check==null)
        {
            $('#add_drproductionDate').parent().addClass("has-error");
            $('#add_drproductionDate').next("span").text("请输入正确格式的日期");
            return false;
        }else {
            $('#add_drproductionDate').parent().addClass("has-success");
            $('#add_drproductionDate').next("span").text("");
        }
        if(drexpirationDate_check==null)
        {
            $('#add_drexpirationDate').parent().addClass("has-error");
            $('#add_drexpirationDate').next("span").text("请输入正确格式的日期");
            return false;
        }else {
            $('#add_drexpirationDate').parent().addClass("has-success");
            $('#add_drexpirationDate').next("span").text("");
        }
        return  true;
    }
    //给新增药品 模态框增加方法
    $('#AllProducts_add').click(function () {
        $('#AllProducts_add_modal').modal({
            backdrop:"static"
        });
    })
    //新增药品 模态框点击事件
    $('#product_modal_add_btn').click(function () {
       if(check_Add_form_Data()==false){
       }else {
           $.ajax({
               url: "${pageContext.request.contextPath}/drug/drugOperation",
               type: 'POST',
               data: $('#AllProducts_add_modal form').serialize(),
               success:function (result) {
                   alert(result.msg);
                   $('#AllProducts_add_modal').modal('hide');
                   to_page(Math.ceil(totalRecord/5));
               }
           })
       }
    })

    //验证id是否重复
    $('#add_drid').change(function () {
        $.ajax({
            url:"${pageContext.request.contextPath}/drug/checkDrug",
            data:"drid="+this.value,
            type:'POST',
            success:function (result) {
                if(result.code==100){  //成功
                    $('#add_resultP').hide()
                }else if(result.code=200){ //失败
                   $('#add_resultP').show();
                }
            }
        })
    });

    $(function(){
        $('.search-bar .dropdown-menu a').click(function() {
            var field = $(this).data('field') || '';
            $('#search-field').val(field);
            $('#search-btn').html($(this).text() + ' <span class="caret"></span>');
        });
    });

    //通过类名给表格中的编辑连接绑定事件
    $(document).on("click",".editProduct_btn",function () {
        // vid 获取此项的id
        let  cid = $(this).attr("id");
        cid  = cid.toString().replace("change","");
        //rid即为要获取的id
        let drid=$("#drid"+cid).text();
        getEmp(drid)
        //将vid放在修改按钮的属性上,这样可以让后端通过vid查找要修改医师
        $('#AllProducts_change_modal_btn').attr("edit_drid",drid)
        $('#AllProducts_change_modal').modal({
            backdrop:"static"
        })
    })
    //拿到点击后表格中id的值,使用Ajax查询信息显示在模态框中
    function getEmp(drid) {
        $.ajax({
            url:"${pageContext.request.contextPath}/drug/findDrugByIds",
            data:"drid="+drid,
            success:function (result) {
                let drug=result.extend.drug;
                $.each(drug,function (index,item) {
                    $('#Drugdrid_update_input').val(item.drid);
                    $('#Drugdrname_update_input').val(item.drname);
                    $('#Drugdrprice_update_input').val(item.drprice);
                    $('#Drugdrnumber_update_input').val(item.drnumber);
                     })
            }
        })
    }
    //根据模态框中的修改按钮修改
    $('#AllProducts_change_modal_btn').click(function () {
        let drid=$(this).attr("edit_drid");
        $.ajax({
            url:"${pageContext.request.contextPath}/drug/updateDrug",
            data:$('#AllProducts_change_modal form').serialize(),
            success:function () {
                $('#AllProducts_change_modal').modal('hide');
                to_page(currentPage);
            }
        })
    })

    //给表格中的删除链接绑定事件
    $(document).on("click",".deleteProduct_btn",function () {
        let cid = $(this).attr("id");
        cid=cid.toString().replace("delete","");
        let drid = $('#drid'+cid).text();
        $.ajax({
            url:"${pageContext.request.contextPath}/drug/deleteDrug",
            data:"drid="+drid,
            success:function (result) {
                to_page(currentPage);
            }
        })
    })
    //给删除按钮绑定事件,批量删除
    $('#AllProducts_delete').click(function () {
        let drids="";
        let drids1="";
        $.each($(".check_item:checked"),function () {
            let cid =$(this).attr("id");
            cid=cid.toString().replace("drugTableCheck","");
            let drid=$('#drid'+cid).text();
            drids+=drid+",";
            drids1+=drid+"-";
        })
        drids=drids.substring(0,drids.length-1);
        drids1=drids1.substring(0,drids1.length-1);
        if(confirm("确认删除医师编号为["+drids+"]的医师吗?")){
            $.ajax({
                url:"${pageContext.request.contextPath}/drug/deleteDrugByIds",
                data:"drids="+drids1,
                success:function (result) {
                    to_page(currentPage);
                }
            })
        }
    })
</script>
</body>
</html>
